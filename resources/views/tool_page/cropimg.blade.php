@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="css/style_crop.css" type="text/css" />
    <style>
        .containerf
        {
            position: absolute;
            top: 10%; left: 10%; right: 0; bottom: 0;
        }

        .cropped>img
        {
            margin-right: 10px;
        }
    </style>
<body>
  <div class="container">
      <div class="row">
          <div class="col-md-10 col-md-offset-1">
              <div class="panel panel-default">
                  <div class="panel-body">
                    <script src="js/cropbox.js"></script>
                    <br><p  align="center">ถ้าท่าน อัพโหลดภาพนี้ ภาพนี้จะใช้เป็นรูปโปรไฟล์ของท่านเเละท่านสามารถเปลี่ยนรูปของท่านได้ทุกเมื่อที่คุณต้องการ</p>
                    <hr>
                    <center>
                      <div class="col-md-12 ">
                        <div class="imageBox">
                            <div class="thumbBox"></div>
                            <div class="spinner" style="display: none">Loading...</div>
                        </div>
                      </div>
                        <br>
                        <form action="{{ url('/savecrop') }}" method="post">
                           <input type="hidden" value="{{csrf_token() }}" name="_token">
                            <input type="file" id="file" style="float:left; width: 250px">
                            <input id="img_path" type="hidden" name="img_path">
                            <input id="db_path" type="hidden" name="db_path">
                            <input id="submit" type="submit" value="Submit">
                        </form>
                      <div class="col-md-12 ">
                              <input  class="btn btn-default" type="button" id="btnZoomIn" value="+" >
                              <input class="btn btn-default" type="button" id="btnZoomOut" value="-" >
                              <input class="btn btn-default" type="button" id="btnCrop" value="Crop" >
                      </div>
                      <div class="col-md-12 ">
                              <hr>
                      </div>


                    </center>
                  </div>
                  <a id="img1" target="_blank">
                  <div class="cropped"></div>

                </div>
          </div>
    </div>
</div>
<script type="text/javascript">
    if(i == 1){
    setTimeout(function(){ location.reload(); }, 10);
    }
    var i = 1;
    document.getElementById('file').style.visibility = "hidden";
    document.getElementById('submit').style.visibility = "hidden";
    window.onload = function() {
        var options =
        {
            imageBox: '.imageBox',
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: '{{Auth::user()->path_img}}'
        }
        var cropper = new cropbox(options);
        document.querySelector('#file').addEventListener('change', function(){
            var reader = new FileReader();
            reader.onload = function(e) {
                options.imgSrc = e.target.result;
                cropper = new cropbox(options);
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        })
        document.querySelector('#btnCrop').addEventListener('click', function(){
            var img = cropper.getDataURL();
            console.log(img);
            document.getElementById("img_path").value = img;
            document.getElementById("db_path").value = '{{Auth::user()->path_img}}';
          //window.location.href = img;
          setTimeout(function(){ document.getElementById('submit').click(); }, 10);
          //  document.querySelector('.cropped').innerHTML += '<img src="'+img+'">';

        })

        document.querySelector('#btnZoomIn').addEventListener('click', function(){
            cropper.zoomIn();
        })
        document.querySelector('#btnZoomOut').addEventListener('click', function(){
            cropper.zoomOut();
        })

    };
</script>

</body>
</html>
@endsection
