@extends('layouts.app')
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="css/texture.css">
<body background="img/bg/11.png">
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5 col-md-offset-7">
            <div class="panel panel-default " style="background:transparent;  border-color:transparent;">
              <div class="panel panel-default " style="background:#fff; position: absolute; width:93.5%; height:97%; opacity:0.9;"></div>
                <div class="panel-body " >
                  <h3 align="center">Register</h3>
                    <form class="form-horizontal" role="form" method="post" action="{{ url('/register') }}">
                        {{ csrf_field() }}
                          <input type="hidden" value="{{csrf_token() }}" name="_token">
                        <!-- Nick-Name -->
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                                <br>Nick-Name<br>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- First-Name -->
                        <div class="form-group{{ $errors->has('fristname') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                                First Name<br>
                                <input id="fristname" type="text" class="form-control" name="fristname" value="{{ old('fristname') }}">

                                @if ($errors->has('fristname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fristname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Last-Name -->
                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                                Last Name<br>
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">
                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Gender -->
                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                                Gender<br>
                                <div class="col-md-6">
                                    <p align="center">
                                      <input class="w3-radio form-control" type="radio" name="gender" value="male" checked>
                                      <label class="w3-validate">Male</label>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                  <p align="center">
                                    <input class="w3-radio form-control" type="radio" name="gender" value="female">
                                    <label class="w3-validate">Female</label>
                                  </p>
                                </div>

                                @if ($errors->has('l-name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('l-name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- birthday -->
                        <div class="form-group{{ $errors->has('Day') ? ' has-error' : '' }}{{ $errors->has('Month') ? ' has-error' : '' }}{{ $errors->has('Year') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                                Birthday<br>
                                <div class="col-md-4">
                                    <p align="center">
                                        <select class="w3-select w3-border" name="Day" id="Day" value="{{ old('Day') }}">
                                          <option value="" disabled selected>Day</option>
                                          @for($i=1;$i<=31 ; $i++)
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                          @endfor
                                        </select>
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <p align="center">
                                        <select class="w3-select w3-border" name="Month"  id="Month" value="{{ old('Month') }}">
                                          <option value="" disabled selected>Month</option>
                                          <option value="1">January</option>
                                          <option value="2">February</option>
                                          <option value="3">March</option>
                                          <option value="4">April</option>
                                          <option value="5">May</option>
                                          <option value="6">June</option>
                                          <option value="7">July</option>
                                          <option value="8">August</option>
                                          <option value="9">September</option>
                                          <option value="10">October</option>
                                          <option value="11">November</option>
                                          <option value="12">December</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <p align="center">
                                        <select class="w3-select w3-border" name="Year" id="Year" value="{{ old('Year') }}">
                                          <option value="" disabled selected>Year</option>
                                          @for($i=1940;$i<=2016 ; $i++)
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                          @endfor
                                        </select>
                                    </p>
                                </div>
                                @if ($errors->has('Day')||$errors->has('Month')||$errors->has('Year'))
                                    <span class="help-block">
                                      <strong>The Birthday field is required.</strong>
                                    </span>
                                @endif
                          </div>
                     </div>
                        <!-- E-Mail Address -->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                                E-Mail Address
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Password -->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                              Password
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!-- Confirm Password -->
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                              Confirm Password
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"><br>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 ">
                              <center>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                                </center>
                            </div>
                        </div>

                    </form>
                </div>

        </div>
    </div>
</div>
</div>
</body>
@endsection
