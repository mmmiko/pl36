
@extends('layouts.app')

@section('content')
<body background="img/bg/11.png">
<div class="container">
    <div class="row">
        <div class="col-md-5 col-md-offset-7 ">
          <div class="panel panel-default " style="background:transparent;  border-color:transparent;">
            <div class="panel panel-default " style="background:#fff; position: absolute; width:93.5%; height:97%; opacity:0.9;"></div>
                <div class="panel-body " style="border-color: #000;">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                              <br>E-Mail<br><br>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                              Password<br><br>
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-1">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-5  ">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>
                                <a  class="btn btn-primary" href="{{ url('/register') }}">
                                      <i class="fa fa-btn fa-user"></i></i> Register
                                </a>
                                <div align="right"><a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a></div>
                            </div>
                        </div>

                    </form>
                </div>
        </div>
    </div>
</div>
</body>
@endsection
