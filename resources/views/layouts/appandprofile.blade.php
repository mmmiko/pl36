<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PL36</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
        .circleborder {
        width: 25px;
        height: 25px;
        border-radius: 150px;
        -webkit-border-radius: 150px;
        -moz-border-radius: 150px;
        background: url(URL) no-repeat;
        box-shadow: 0 0 8px rgba(0, 0, 0, .8);
        -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
        -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
}
    </style>
</head>

<body id="app-layout" style="padding-top:0px;  ">
    <nav class="navbar navbar-inverse navbar-static-top dropsha" >
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    PL36
                </a>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    @if (Auth::guest())
                        <li><a href="{{ url('/home') }}">Home</a></li>
                    @else
                        <li><a href="{{ url('/home') }}">Home</a></li>
                          @if( Auth::user()->mysite == !null)
                            <li><a href="{{ url('/home') }}">Mysite</a></li>
                          @endif
                          @if( Auth::user()->myfile == !null)
                            <li><a href="{{ url('/home') }}">Myfile</a></li>
                          @endif
                          @if( Auth::user()->myphoto == !null)
                            <li><a href="{{ url('/home') }}">Myphoto</a></li>
                          @endif
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                         @if( Auth::user()->path_img == null)
                        <li><a href="{{ url('/1') }}">
                               <img class="circleborder" src="img/texture/default profile picture.png" width="25px" height="25px">
                        </a></li>
                        @else
                        <li><a href="{{ url('/home') }}">
                               <img class="circleborder" src="{{ Auth::user()->path_img }}" width="25px" height="25px">
                        </a></li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/changeimg') }}"><i class="fa fa-btn fa-user"></i>Edit Profile</a></li>
                                <li><a href="{{ url('/service') }}"><i class="fa fa-btn fa-cog"></i>Setting Service</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>

                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
<!-- --------------------------------------------------------------------------------------------------end layout--------------------------------------------------------------------------------------------------- -->
<?php
use App\customprofile;
$custom = customprofile::find(Auth::user()->id);
 ?>
 <!-- tool color -->
     <link rel="stylesheet" type="text/css" href="spectrum.css">
     <script type="text/javascript" src="docs/jquery-1.9.1.js"></script>
     <script type="text/javascript" src="spectrum.js"></script>
     <script type='text/javascript' src='docs/toc.js'></script>
     <script type='text/javascript' src='docs/docs.js'></script>
 <!-- tool color -->
 <script src="jscolor.js"></script>
 <!-- popup -->
 <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
 <link href="http://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.css" rel="stylesheet" type="text/css">
 <link rel="stylesheet" href="css/profile style.css">
 <link rel="stylesheet" href="css/popup.css">


 <body background ="{{ $custom->bg }}" >
 <!-- popup tool  color -->
   <!-- สีเส้นขอบ 1 -->
   <div id="popup1" class="modal-box" >
       <a href="#" class="js-modal-close close">×</a><br>
       <form action="{{url('/show')}}" method="post">
           <div class="panel-body form-group">
             <center>เปลียนสีเส้น <B>กรอบนอก</B></center>
             <hr><br>
             <center>
             <input type="hidden" value="{{csrf_token() }}" name="_token">
             <div id="border" class="panel panel-default" style="width:200px; height:200px; background-color:{{ $custom->linecolor2 }};border-color:{{ $custom->linecolor }};"></div><br>
             <input  id="c1" class="form-control input-lg" value="000000" style="width:100px;" name="colorborder"><br>
             <button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'} btn btn-info">
               Pick color
             </button><br>
             <input type="hidden" name="check" value="1">
             </center>
           </div>
           <div class="panel-body" align="right">
             <hr>
             <input class="btn btn-info" type="submit"  value="ยืนยัน" >
           </div>
     </form>

   </div>
   <!-- สีเส้นขอบ 2 -->
   <div id="popup2" class="modal-box" >
       <a href="#" class="js-modal-close close">×</a><br>
       <form action="{{url('/show')}}" method="post">
           <div class="panel-body form-group">
             <center>เปลียนสีเส้น <B>กรอบใน</B></center>
             <hr><br>
             <center>
             <input type="hidden" value="{{csrf_token() }}" name="_token">
             <div id="bg" class="panel panel-default" style="width:200px; height:200px; background-color:{{ $custom->linecolor2 }};border-color:{{ $custom->linecolor }};"></div><br>
             <input  id="c2" class="form-control input-lg" value="000000" style="width:100px;" name="colorbg"><br>
             <button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'} btn btn-info">
               Pick color
             </button><br>
             <input type="hidden" name="check" value="2">
             </center>
           </div>
           <div class="panel-body" align="right">
             <hr>
             <input class="btn btn-info" type="submit"  value="ยืนยัน" >
           </div>
     </form>
   </div>
   <script>
   function setTextColor(picker) {
     console.log('#' + picker.toString());
     document.getElementById('bg').style.backgroundColor = '#' + picker.toString()
     document.getElementById('border').style.borderColor = '#' + picker.toString()
     document.getElementById('c1').value = '#' + picker.toString()
     document.getElementById('c2').value = '#' + picker.toString()
   }
   </script>
 <!-- popup tool  -->

 <div class="container-fluid">
     <div class="row">
       <!-- โปรไฟล์ -->
       <div class="col-md-10 col-md-offset-1">

           <div class="panel panel-default" style="background-color:{{ $custom->linecolor2 }};border-color:{{ $custom->linecolor }};">
             <div class="topleft">
               <a class="buttonedit " data-modal-id="popup1"><i class="fa fa-btn fa-pencil-square-o"></i></a>
             </div>
             <div class="topleft2">
               <a class="buttonedit " data-modal-id="popup2"><i class="fa fa-btn fa-pencil-square-o"></i></a>
             </div>
               <!-- <div class="panel-heading">Dashboard</div> -->
               <div class="panel-body">
                 <!-- รูปหน้าปก -->
                   <img src="{{ $custom->cover }}"  style="object-fit: cover;  width: 100%; height:300px;">
                 <!-- รูปหน้าปก -->
                 <!-- ปุ่มแก้ไขรูปหน้าปกขวาบน -->
                   <div class="topright">
                     <a class="buttonedit" style="opacity:0.5;" href="{{ url('/customcover')}}"><i class="fa fa-btn fa-pencil-square-o"></i></a>
                   </div>
                 <!-- ปุ่มแก้ไขขวาบน -->
                 <div class="bottomleft">
                   <a class="buttonedit2" style="opacity:0.5;"  href="{{ url('/custombg')}}"><i class="fa fa-btn fa-pencil-square-o"></i>Background </a>
                   <a class="buttonedit2" style="opacity:0.5;"  href="{{ url('/customtexture')}}"><i class="fa fa-btn fa-pencil-square-o"></i>Texture </a>
                 </div>
                 <!-- รูปโปรไฟล์ เเละ ชื่อ-->
                   <div class="center">
                     <!-- รูปโปรไฟล์-->
                       <img class="circleborder" id="img"  src="{{Auth::user()->path_img}}" style=" width:150px; height: 150px;">
                     <!-- ปุ่มแก้ไขรูปโปรไฟล์-->
                       <div class="center"><br>
                          <a class="buttonedit" style="opacity:0.5;" href="{{ url('/changeimg') }}">
                            <i class="fa fa-btn fa-pencil-square-o"></i>
                          </a>
                       </div>
                     <br><br>
                     <!--ชื่อ-->
                     <div class="panel panel-default" style="width:100%;background-color:hsla(100,100%,100%,0.5);">
                       <center>{{Auth::user()->fristname}}&nbsp;&nbsp;&nbsp;{{Auth::user()->lastname}}<br>({{Auth::user()->name}})</center>
                     </div>
                   </div>
                 <!-- รูปโปรไฟล์ เเละ ชื่อ-->
               </div>
           </div>
       </div>
       <!-- โปรไฟล์ -->
 <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
 <script>
 $(function(){

 var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

 	$('a[data-modal-id]').click(function(e) {
 		e.preventDefault();
     $("body").append(appendthis);
     $(".modal-overlay").fadeTo(500, 0.7);
     //$(".js-modalbox").fadeIn(500);
 		var modalBox = $(this).attr('data-modal-id');
 		$('#'+modalBox).fadeIn($(this).data());
 	});


 $(".js-modal-close, .modal-overlay").click(function() {
     $(".modal-box, .modal-overlay").fadeOut(500, function() {
         $(".modal-overlay").remove();
     });

 });

 $(window).resize(function() {
     $(".modal-box").css({
         top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
         left: ($(window).width() - $(".modal-box").outerWidth()) / 2
     });
 });

 $(window).resize();

 });
 </script>
 <script type="text/javascript">

   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-36251023-1']);
   _gaq.push(['_setDomainName', 'jqueryscript.net']);
   _gaq.push(['_trackPageview']);

   (function() {
     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
 S
 </script>














</body>
</html>
