<!DOCTYPE html>
<?php
use App\service;
?>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PL36</title>
  <!-- Fonts -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
  <!-- Styles -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- table -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" >

  <!-- เว้นวรรค -->
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
        .fl{
          float: left;
        }
        #space{
          margin-right:  20px;
        }
        .circleborder {
            width: 25px;
            height: 25px;
            border-radius: 150px;
            -webkit-border-radius: 150px;
            -moz-border-radius: 150px;
            background: url(URL) no-repeat;
            box-shadow: 0 0 8px rgba(0, 0, 0, .8);
            -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
            -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
        }
    </style>
      <!-- เว้นวรรค -->
      <!-- Tab -->
    <style>
    body {font-family: "Lato", sans-serif;}

    ul.tab {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        border: 0px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Float the list items side by side */
    ul.tab li {float: left;}

    /* Style the links inside the list items */
    ul.tab li a {
        display: inline-block;
        color: black;
        text-align: center;
        padding: 8px 8px;
        text-decoration: none;
        transition: 0.3s;
        font-size: 18px;
    }

    /* Change background color of links on hover */
    ul.tab li a:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    ul.tab li a:focus, .active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        -webkit-animation: fadeEffect 1s;
        animation: fadeEffect 1s;
    }

    @-webkit-keyframes fadeEffect {
        from {opacity: 0;}
        to {opacity: 1;}
    }

    @keyframes fadeEffect {
        from {opacity: 0;}
        to {opacity: 1;}
    }
    </style>
  <!-- Tab -->
  <!-- uploadfile -->
    <style>
    .js .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    .inputfile + label {
        max-width: 80%;
        font-size: 1.25rem;
        /* 20px */
        font-weight: 700;
        text-overflow: ellipsis;
        white-space: nowrap;
        cursor: pointer;
        display: inline-block;
        overflow: hidden;
        padding: 0.625rem 1.25rem;
        /* 10px 20px */
    }

    .no-js .inputfile + label {
        display: none;
    }

    .inputfile:focus + label,
    .inputfile.has-focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }


    /* style 4 */
    .inputfile-4:focus + label,
    .inputfile-4.has-focus + label,
    .inputfile-4 + label:hover {
        color: #722040;
    }
    .inputfile-4 + label figure {
        width: 100px;
        height: 100px;
        border-radius: 50%;
        background-color: #d3394c;
        display: block;
        padding: 20px;
        margin: 0 auto 10px;
    }

    .inputfile-4 + label svg {
        width: 100%;
        height: 100%;
        fill: #f1e5e6;
    }

    </style>
    <script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
    <!-- uploadfile -->

  <style>
  #search input[type="text"] {
      background: url(search-dark.png) no-repeat 10px 6px #444;
      border: 0 none;
      font: bold 12px Arial,Helvetica,Sans-serif;
      color: #777;
      width: 150px;
      padding: 6px 15px 6px 35px;
      -webkit-border-radius: 20px;
      -moz-border-radius: 20px;
      border-radius: 20px;
      text-shadow: 0 2px 2px rgba(0, 0, 0, 0.3);
      -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
      -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
      box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
      -webkit-transition: all 0.7s ease 0s;
      -moz-transition: all 0.7s ease 0s;
      -o-transition: all 0.7s ease 0s;
      transition: all 0.7s ease 0s;
  }
  #search input[type="text"]:focus {
    width: 200px;
  }
      body {
          font-family: 'Lato';
      }

      .fa-btn {
          margin-right: 6px;
      }
      .circleborder {
      width: 30px;
      height: 30px;
      border-radius: 150px;
      -webkit-border-radius: 150px;
      -moz-border-radius: 150px;
      background: url(URL) no-repeat;
      box-shadow: 0 0 8px rgba(0, 0, 0, .8);
      -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
      -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
}
  </style>

</head>

<body id="app-layout" style="padding-top:0px;">
  <div  >
    <nav class="navbar navbar-inverse navbar-static-top navbar-fixed-top">
        <div class="container" >
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}" style="padding: 0px 0px 0px 5px;">
                    <img src="img/icon/pl36.png" width="50px"  >
                </a>
                @if (Auth::guest())
                    <!-- <li><a href="{{ url('/home') }}">Home</a></li> -->
                @else
                <ul class="nav navbar-nav" style=" padding: 8px 0px 0px 0px;">

                        &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;
                        <input  name="q" type="text" size="25" placeholder="Search..." />

                </ul>
                @endif
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    @if (Auth::guest())
                        <!-- <li><a href="{{ url('/home') }}">Home</a></li> -->
                    @else
                    <?php $DB = service::find(Auth::user()->id); ?>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <?php if($DB != ''){ ?>
                               @if( Auth::user()->path_img == null)
                              <li><a href="{{ url('/1') }}">
                                     <img class="circleborder" src="img/texture/default profile picture.png" width="25px" height="25px">
                                   </a>
                              </li>
                              @else
                              <li >
                                  <a href="{{ url('/home') }}"  >
                                    <div style="position: absolute; top: 7px;">
                                     <img class="circleborder" src="{{ Auth::user()->path_img }}" width="30px" height="30px" >
                                   </div>
                                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ Auth::user()->fristname }}
                                   </a>
                              </li>

                              <?php if($DB->mysite==1){ ?>
                              <li ><a href="{{ url('/mysite') }}"  >
                                     |&nbsp;&nbsp;<i class="glyphicon glyphicon-modal-window"></i>
                                   </a>
                              </li>
                              <?php } ?>
                              <?php if($DB->myfile==1){ ?>
                              <li ><a href="{{ url('/myflie') }}"  >
                                     <i class="glyphicon glyphicon-folder-open"></i>
                                   </a>
                              </li>
                              <?php } ?>
                              <?php if($DB->myphoto==1){ ?>
                              <li ><a href="{{ url('/myphoto') }}"  >
                                     <i class="glyphicon glyphicon-picture"></i>&nbsp;&nbsp;|
                                   </a>
                              </li>
                              <?php } ?>
                              @endif
                        <?php } ?>
                        <!-- เพื่อน -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="glyphicon glyphicon-user"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>Emtry</li>
                            </ul>
                        </li>
                        <!-- แจ้งเตือน -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="glyphicon glyphicon-globe"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>Emtry</li>
                            </ul>
                        </li>
                        <!-- ตั้งค่า -->
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="glyphicon glyphicon-cog"></i> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/changeimg') }}"><i class="fa fa-btn fa-user"></i>Edit Profile</a></li>
                                <li><a href="{{ url('/service') }}"><i class="fa fa-btn fa-cog"></i>Setting Service</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
  </div><br><br>

    @yield('content')

        <!-- JavaScripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <!-- table -->
    <script src="https://code.jquery.com/jquery-1.12.3.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <!-- table -->

    <!-- uploadfile -->
    <script>

    ;( function ( document, window, index )
    {
    	var inputs = document.querySelectorAll( '.inputfile' );
    	Array.prototype.forEach.call( inputs, function( input )
    	{
    		var label	 = input.nextElementSibling,
    			labelVal = label.innerHTML;
  console.log(label);
    		input.addEventListener( 'change', function( e )
    		{
    			var fileName = '';
    			if( this.files && this.files.length > 1 )
    				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
    			else
    				fileName = e.target.value.split( '\\' ).pop();

    			if( fileName )
    				label.querySelector( 'span' ).innerHTML = fileName;
    			else
    				label.innerHTML = labelVal;
    		});

    		// Firefox bug fix
    		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
    		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
    	});
    }( document, window, 0 ));
    </script>
    <!-- uploadfile -->
    <!-- table -->
    <script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
    </script>
    <!-- table -->
</body>
</html>
