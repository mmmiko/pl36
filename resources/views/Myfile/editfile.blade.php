@extends('layouts.app')

@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<style type="text/css">
div.img-resize img {
	width: 100px;
	height: auto;
}

div.img-resize {
	overflow: hidden;
	text-align: center;
}
</style>
<div class="container">
    <div class="row">

        <div class="col-md-12 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">ManageFile </div>
                <div class="panel-body">

                  <table id="example" class="table table-condensed" data-click-to-select="true" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                              	 <th><input type="checkbox" id="checkall" /></th>
                                  <th style="width:30%">NameFile</th>
                                  <th style="width:10%">TypeFile</th>
                                  <th style="width:10%">SizeFile</th>
                                  <th style="width:15%">DateTimeUpFile</th>
                                  <th style="width:30%">Action</th>
                              </tr>
                          </thead>

                          <tbody>

                            @foreach ($objs as $key)
                              <tr>
                                  <td>
                              	     <input type="checkbox" id="selDel"  onclick="delselect('{{$key->myfile_id}}')" class="checkthis" name="sel"  />
                                  </td>
                                  <td>  {{$key->faceName}}</td>
                                  <td>  {{$key->typefile}}</td>
                                  <td>  {{$key->sizefile}}</td>
                                  <td>  {{$key->date_upflie}}</td>
                                  <td>
                                    <form action="{{url('edit/')}}" enctype="multipart/form-data" method="post" >
                                          {{ csrf_field() }}
                                          <input type="hidden" name="idfile" value="{{$key->myfile_id}}">
                                          <button type="submit"  class="fl btn btn-info glyphicon glyphicon-cog" id="space"> </button>
                                    </form>
																		<form action="{{url('editmyflie/delete/')}}" enctype="multipart/form-data" method="post" >
																			{{ csrf_field() }}
																			<input type="hidden" name="idfile" value="{{$key->myfile_id}}">
																			<input type="hidden" name="iduse" value="{{$key->id}}">
																			<button type="submit"  class="fl btn btn-danger glyphicon glyphicon-trash" id="space" onclick="return confirm('are you sure?')"> </button>
																			<!-- <a  class="fl btn btn-danger glyphicon glyphicon-trash" id="space" href="{{url('editmyflie/'.$key->myfile_id.'/'.$key->id)}}"  onclick="return confirm('are you sure?')"> </a> -->
																		</form>
																	<form action="{{url('editmyflie/download/')}}" enctype="multipart/form-data" method="post" >
																		{{ csrf_field() }}
																		<input type="hidden" name="idfile" value="{{$key->myfile_id}}">
																		<input type="hidden" name="limit" value="Auth::user()->limitspeed">
																		<button type="submit"  class="fl btn btn-success glyphicon glyphicon-download-alt" id="space"> </button>
																	<!-- <a  class="fl btn btn-success glyphicon glyphicon-download-alt" id="space" download="{{ $key->faceName }}" href="{{url('editmyflie/download/'.Auth::user()->limitspeed.'/'.$key->myfile_id)}}"> </a> -->
																	</form>
																	</td>
                              </tr>
                              @endforeach
                          </tbody>

                      </table>
											<hr>
                      <a href="{{url('myflie/')}}"class="fl btn btn-default btn-sm glyphicon glyphicon-circle-arrow-left" id="space"> Back</a>
											<form action="{{url('delall/')}}" enctype="multipart/form-data" method="post" >
                      <!-- <a href="{{url('myflie/')}}"class="fl btn btn-danger btn-sm glyphicon glyphicon-circle-arrow-left" id="space"onclick="return confirm('are you sure?')"> DeleteAll</a> -->
														{{ csrf_field() }}
														<input type="hidden" name="delA" id="delId">
														<input type="hidden" name="seclId" id="seclId">
														<input type="hidden" name="coutDel" id="coutDel">
														<input type="hidden" name="idUser" value="{{$objs[0]->id}}">
														<button type="submit" id="myBtn" class="fl btn btn-danger btn-sm glyphicon glyphicon-trash" onclick="return confirm('are you sure?')" disabled> DeleteAll</button>
											</form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
var del = "";
$(document).ready(function(){
$("#example #checkall").click(function () {
        if ($("#example #checkall").is(':checked')) {
            $("#example input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
							document.getElementById("delId").value = "delAll";
							document.getElementById("myBtn").disabled = false;
            });

        } else {
            $("#example input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
									document.getElementById("myBtn").disabled = true;
            });
        }
    });
    $("[data-toggle=tooltip]").tooltip();

});
var idsel ="";
var Coutdel=0;
function delselect(Id) {
	var checkedValue = $('.checkthis:checked').val();
	if(	checkedValue == "on"){

		document.getElementById("myBtn").disabled = false;
		idsel += ":"+Id;
	  Coutdel += 1;
		console.log(idsel);
	}
	else{
		document.getElementById("myBtn").disabled = true;
	}
	document.getElementById("seclId").value = idsel;
  document.getElementById("coutDel").value = Coutdel;


}
</script>
@endsection
