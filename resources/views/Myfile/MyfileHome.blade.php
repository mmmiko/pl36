@extends('layouts.app')

@section('content')

<div class="container">
      <div class="row">
        <div class="col-md-4 ">
          <div class="panel panel-default">
            <div class="panel-heading">UploadFile</div>
              <div class="panel-body">
                <a  class="fl btn btn-info glyphicon glyphicon-upload" id="space" href="{{url('create/')}}"> Upload</a>
                  @if (count($a) != 0)
                          <a  class="fl btn btn-primary glyphicon glyphicon-th-list" id="space" href="{{url('editmyflie/')}}"> ManageFile</a>
                  @endif
              </div>
            </div>
          </div>

          <div class="col-md-8">
              <div class="panel panel-default">
                  <div class="panel-heading">Welcome Myfile</div>
                  <div class="panel-body">
                  @if (count($a) != 0)

                  <table id="example" class="table table-condensed" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th style="width:5%">#</th>
                                  <th style="width:40%">NameFile</th>
                                  <th style="width:10%">SizeFile</th>
                                  <th style="width:20%">TypeFile</th>
                                  <th style="width:25%">DateTimeUpFile</th>

                              </tr>
                          </thead>

                          <tbody>
                              <?php $i=0; ?>
                              @foreach ($a as $file)
                              <tr>
                                  <td>  <?php echo ++$i; ?></td>
                                  <td>  {{$file->faceName}}</td>
                                  <td>  {{$file->sizefile}}</td>
                                  <td>  {{$file->typefile}}</td>
                                  <td>  {{$file->date_upflie}}</td>
                              </tr>
                              @endforeach
                          </tbody>
                    </table>
                    @else
                          <center><p class="help-block">You have no file.</p></center>
                    @endif

                </div>
              </div>
              </div>
              </div>
          </div>
      </div>
    </div>
</div>
@endsection
