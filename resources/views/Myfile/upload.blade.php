@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">File Upload </div>
                <div class="panel-body">

                  <form action="{{$url}}" enctype="multipart/form-data" method="post" >
                        {{method_field($method)}}
                        <div class="form-group">
                          <center>
                          <input type="hidden" name="iduser" value="{{ Auth::user()->id }}">
                          <input type="hidden" name="idname" value="{{ Auth::user()->name }}">
                          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                          <div class="box">
                              <input type="file" name="file[]" id="file-5" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple />
                              <label for="file-5">
                                  <img  img src="img/upAll.png" alt="Smiley face" height="100%" width="100" class="img-rounded">
                                  <br>
                                 <span>Choose image&hellip;</span>
                              </label>
                          </div>
                              @if(Session::has('upfile'))
                               <span class="help-block">
                                 <strong>{!! Session::get('upfile') !!}</strong>
                               </span>
                              <hr>
                              @endif
                          </center>
                      </div>
                        <div class="col-md-2 col-md-offset-4">
                          <button type="submit" id="space" class="fl btn btn-primary btn-sm glyphicon glyphicon-floppy-disk" > Save</button>
                       </div>
                  </form>
                  <form action="{{$url}}">
                          <button type="submit"  class="fl btn btn-danger btn-sm glyphicon glyphicon-remove-circle" > Cancle</button>
                  </form>


                </div>
                    <div class="panel-heading ">
                      <div class="glyphicon glyphicon-asterisk"> Notes </div>
                    </div>
                  <div class="panel-body">
                      <div class="form-group">
                          <p>&#183;	You can uploadMutifile.</p>
                        <p>&#183;	The maximum file size for uploads in this demo is 750MB (default file size is unlimited).</p>
                        <p>&#183;	Uploaded files will be deleted automatically after 5 minutes or less (demo files are stored in memory).</p>
                        <p>&#183;	You can drag & drop files from your desktop on this webpage (see Browser support).</p>
                      </div>
                  </div>
            </div>
        </div>
    </div>
</div>

@endsection
