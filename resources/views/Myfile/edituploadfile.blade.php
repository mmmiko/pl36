@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">EditFile </div>
                <div class="panel-body">

                <form action="{{$url}}"  enctype="multipart/form-data" method="post" >
                      {{method_field($method)}}
                      {{ csrf_field() }}
                      <input type="hidden" name="iduser" value="{{ Auth::user()->id }}">

                  <div class="form-group{{ $errors->has('NameFile') ? ' has-error' : '' }}">
                      <label for="NameFile" class="col-md-2 control-label">NameFile</label>
                      <div class="col-md-6">
                          <input type="text"  class="form-control"  id="NameFile" placeholder="NameFile" name="NameFile" value="{{ old('NameFile') }}">
                          @if ($errors->has('NameFile'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('NameFile') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                        <button type="submit" id="space" class="fl btn btn-success btn-sm glyphicon glyphicon-ok-sign" > Submit</button>
                  </form>
                  <form action="{{url('editmyflie/')}}">
                      <button type="submit" id="space" class="fl btn btn-danger btn-sm glyphicon glyphicon-remove-circle" > Back</button>
                  </form>


              </div>
            </div>
        </div>
    </div>
</div>

@endsection
