@extends('layouts.app')
@section('content')

           <!-- if user don't have imgae -->
              <div class="container">
                  <div class="row">
                      <div class="col-md-10 col-md-offset-1">
                          <div class="panel panel-default">
                              <div class="panel-body">
                                <!-- upload file -->
                                <br><p  align="center">ถ้าท่าน อัพโหลดภาพนี้ ภาพนี้จะใช้เป็นรูปโปรไฟล์ของท่านเเละท่านสามารถเปลี่ยนรูปของท่านได้ทุกเมื่อที่คุณต้องการ</p>
                                <hr>
                                <center>

                                    <img class="circleborder" id="img"  src="" style=" width: 25%; height: 25%;">

                                 </center>

                                <form action="">
                                  <input type="hidden" value="{{csrf_token() }}" name="_token">
                                    <div class="col-md-6 col-md-offset-3">
                                      <hr>
                                        <center>
                                          <label class="fileContainer"><input type="file" name="files" id="files" class="btn btn-default" ></label>
                                        </center>
                                        <!-- progress bar -->
                                        <div class="col-md-10 col-md-offset-0" style="padding-top: 10px;">
                                          <div class="progress progress-striped active" >
                                              <div id="re" class="progress-bar" style="width:0%;"></div>
                                          </div>
                                        </div>
                                          <center>  <button type="submit" class="btn btn-default">Upload</button></center>
                                        <!-- progress bar -->

                                    </div>
                                </from>
                                <div class="col-md-2 col-md-offset-10">
                                    <div align="right"><a class="btn btn-success" href="{{ url('/home') }}">ยืนยัน</a></div>
                                </div>
                              </div>

                              </div>
                          </div>
                      </div>
                  </div>
                  <a id="togo" href="{{ url('/cropimg') }}"></a>
              </div>
              <script>
                  //---------------------progress bar---------------------------
                    var img = '{{Auth::user()->path_img}}';
                      document.getElementById("img").src=img;
                  //  document.getElementById("img").src=img;s
                    var form = document.querySelector('form');
                    var request = new XMLHttpRequest();
                    form.addEventListener('submit',function(e){
                        e.preventDefault();
                        $form = $(this);
                        request.upload.addEventListener('progress',function(e){
                            var percent = e.loaded/e.total *100;
                            console.log(percent);
                            console.log(1);
                            $form.find('.progress-bar').width(percent+'%') //push percent to progress-bar
                            if(percent == 100){
                              console.log(img);
                              setTimeout(function(){ document.getElementById('togo').click(); }, 900);
                              //setTimeout(function(){ location.reload(); }, 900);
                              document.getElementById("img").src=img;
                            }
                        });
                        request.addEventListener('load',function(e){
                          $form.find('.progress-bar');
                        });
                        var formdata = new FormData(form);
                        request.open('post','submit'); //send to form where?? ->submit
                        request.send(formdata);
                    });

              </script>
               <!--end if user don't have imgae -->

	<script src="js/custom-file-input.js"></script>
@endsection
