@extends('layouts.app')
<?php
use App\service;
  $DB = service::find(Auth::user()->id);
 ?>
@section('content')
<link rel="stylesheet" href="css/service.css">
           <!-- if user don't have imgae -->
              <div class="container">
                  <div class="row">
                      <div class="col-md-10 col-md-offset-1">
                          <div class="panel panel-default">
                              <div class="panel-body">
                                <!-- upload file -->
                                  <br><p  align="center">ในส่วนนี้ท่านสามารถเปิดบริการต่าง ๆ ที่ PL36 ได้เปิดให้ท่านใช้ได้ฟรี โดยไม่มีค่าใช้จ่ายใด ๆ ได้เเก่ MYSITE,MYFILE เเละ MYPHOTO</p>
                                  <hr>
                                    <!-- submit -->
                                        <form action="{{ url('/saveservice') }}" method="get">
                                          <input type="hidden" value="{{csrf_token() }}" name="_token">
                                            <!-- app  -->
                                              <!-- app is default_profile -->
                                            <?php if($DB == null){ ?>
                                                      <div class="col-md-9 col-md-offset-1" >
                                                          <div class="col-md-3 col-md-offset-1 " >
                                                              <center>
                                                                <img src="img/icon/1.png" class="img-rounded ex1" width="110px" height="110px" ><br>
                                                                MYSITE<br>
                                                                <label class="switch">
                                                                  <input  id="mysite" name="mysite" type="checkbox" value="1" checked="checked">
                                                                  <div class="slider round"> </div>
                                                                </label>
                                                              </center>
                                                          </div>
                                                          <div class="col-md-3 col-md-offset-1" >
                                                              <center>
                                                                <img src="img/icon/2.png" class="img-rounded ex1" width="110px" height="110px" ><br>
                                                                MYFILE<br>
                                                                <label class="switch">
                                                                  <input id="myfile" name="myfile" type="checkbox" value="1">
                                                                  <div class="slider round"> </div>
                                                                </label>
                                                              </center>
                                                          </div>
                                                          <div class="col-md-3 col-md-offset-1" >
                                                              <center>
                                                                <img src="img/icon/3.png" class="img-rounded ex1" width="110px" height="110px" ><br>
                                                                MYPHOTO<br>
                                                                <label class="switch">
                                                                  <input id="myphoto" name="myphoto" type="checkbox" value="1">
                                                                  <div class="slider round"> </div>
                                                                </label>
                                                              </center>
                                                          </div>
                                                      </div>
                                                  <!-- app is default_profile -->
                                          <?php }else{ ?>
                                                    <div class="col-md-9 col-md-offset-1" >
                                                       <!-- mysite check -->
                                                        <?php if($DB->mysite == 1){ ?>
                                                        <div class="col-md-3 col-md-offset-1 " >
                                                            <center>
                                                              <img src="img/icon/1.png" class="img-rounded ex1" width="110px" height="110px" ><br>
                                                              MYSITE<br>
                                                              <label class="switch">
                                                                <input  id="mysite" name="mysite" type="checkbox" value="1" checked="checked">
                                                                <div class="slider round"> </div>
                                                              </label>
                                                            </center>
                                                        </div>
                                                        <?php }else{ ?>
                                                        <div class="col-md-3 col-md-offset-1 " >
                                                            <center>
                                                              <img src="img/icon/1.png" class="img-rounded ex1" width="110px" height="110px" ><br>
                                                              MYSITE<br>
                                                              <label class="switch">
                                                                <input  id="mysite" name="mysite" type="checkbox" value="1">
                                                                <div class="slider round"> </div>
                                                              </label>
                                                            </center>
                                                        </div>
                                                        <?php } ?>
                                                        <!--close mysite check -->
                                                        <!-- myfile check -->
                                                        <?php if($DB->myfile == 1){ ?>
                                                        <div class="col-md-3 col-md-offset-1" >
                                                            <center>
                                                              <img src="img/icon/2.png" class="img-rounded ex1" width="110px" height="110px" ><br>
                                                              MYFILE<br>
                                                              <label class="switch">
                                                                <input id="myfile" name="myfile" type="checkbox" value="1" checked="checked">
                                                                <div class="slider round"> </div>
                                                              </label>
                                                            </center>
                                                        </div>
                                                        <?php }else{ ?>
                                                        <div class="col-md-3 col-md-offset-1" >
                                                            <center>
                                                              <img src="img/icon/2.png" class="img-rounded ex1" width="110px" height="110px" ><br>
                                                              MYFILE<br>
                                                              <label class="switch">
                                                                <input id="myfile" name="myfile" type="checkbox" value="1">
                                                                <div class="slider round"> </div>
                                                              </label>
                                                            </center>
                                                        </div>
                                                        <?php } ?>
                                                        <!--close myfile check -->
                                                        <!-- myphoto check -->
                                                        <?php if($DB->myphoto == 1){ ?>
                                                        <div class="col-md-3 col-md-offset-1" >
                                                            <center>
                                                              <img src="img/icon/3.png" class="img-rounded ex1" width="110px" height="110px" ><br>
                                                              MYPHOTO<br>
                                                              <label class="switch">
                                                                <input id="myphoto" name="myphoto" type="checkbox" value="1" checked="checked">
                                                                <div class="slider round"> </div>
                                                              </label>
                                                            </center>
                                                        </div>
                                                        <?php }else{ ?>
                                                        <div class="col-md-3 col-md-offset-1" >
                                                            <center>
                                                              <img src="img/icon/3.png" class="img-rounded ex1" width="110px" height="110px" ><br>
                                                              MYPHOTO<br>
                                                              <label class="switch">
                                                                <input id="myphoto" name="myphoto" type="checkbox" value="1">
                                                                <div class="slider round"> </div>
                                                              </label>
                                                            </center>
                                                        </div>
                                                        <?php } ?>
                                                        <!--close myphoto check -->
                                                    </div>
                                          <?php } ?>

                                            <!-- app  -->
                                              <br><br><br><br><br><br><br><br><br>
                                              <div class="col-md-12 col-md-offset-0" >
                                                <div align = 'right'>
                                                <input type="submit" class="btn btn-success" value="ยืนยัน">
                                              </div>
                                              </div>
                                              </form>
                                          <!-- submit -->
                                          <br>

                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
@endsection
