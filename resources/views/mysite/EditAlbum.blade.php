@extends('layouts.app')
@section('content')
<style>
div.img {
  display: inline-block;
  position: relative;
  margin: 5px;
  border: 1px solid #ccc;
  float: left;
  width: 167px;
}


div.desc {
    padding: 10px;
    text-align: right;
}


.del_photo {
    display: inline-block;
    float:right;
	  margin:5px 5px 0 0;
    position: absolute;
    top: 0;
    right: 0;
}
</style>
<div class="container">
<div class="row">
      <div class="col-md-12 ">
          <div class="panel panel-default">
              <div class="panel-heading">
                  <a href="{{url('mysite/')}}"class="close" onclick="return confirm('Cancel Edit Photo?')"> &times;</a>
                Edit Album
              </div>


              <div class="panel-body">


              <form action="{{$url}}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" value="{{$objAlbum[0]->IdPostAlbums}}" name="idAb" >
                <input type="hidden"  name="delimg" id="delId">
                <input type="hidden"  name="coutDel" id="coutDel">

                <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group">
                                             <input type="text" class="form-control" value="{{$objAlbum[0]->namalbum}}" name="NameAlbum" placeholder="Untited Album">
                                             <textarea class="form-control" placeholder="Say Somthing about this album..."  name="Description" rows="4" value="" style="width:100%; resize: none;">{{$objAlbum[0]->description}}
                                             </textarea>

                                    <!-- <button id="bdShow" type="button" id="space" class="fl btn btn-default btn-sm help-block" >Event </button> -->
                                    <hr>
                                    <p class="help-block">Change Date</p>
                                    <input type="date" name="bday">
                            </div>
                        </div>
                        <div class="col-md-6 ">
                              <button type="submit" id="space" class="fl btn btn-primary btn-sm ">Done</button>
                  </form>
                              <form action="{{$url}}" method="post">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="check" value="delAll">
                                    <input type="hidden" name="idfordel" value="{{$objAlbum[0]->IdPostAlbums}}">.
                                      <input type="hidden" name="iduse" value="{{$objAlbum[0]->id}}">
                                    <button  type="submit" id="space" class="fl btn btn-default btn-sm glyphicon glyphicon-trash" onclick="return confirm('are you sure?')"></button>
                              </form>
                        </div>
                  </div>
                  <hr>
                  <div class="row">
                        <div class="col-md-12 ">
                                               @foreach ($objAlbum as $tmp)
                                                  <div class="img" id="images{{$tmp->IdAlbum}}">
                                                      <?php if($tmp->typefile =="image/png" ||$tmp->typefile =="image/jpeg"  ||$tmp->typefile =="image/gif") {?>
                                                            <img id="img{{$tmp->IdAlbum}}"  src="{{URL::to('/') }}/{{$tmp->pathimg}}/{{ $tmp->nameimg}}"  width="165" height="160"/>
                                                            <a class="del_photo" id="hidImg{{$tmp->IdAlbum}}" onclick="hedeImg('{{$tmp->IdAlbum}}')">  <i class="glyphicon glyphicon-remove"></i></a>
                                                      <?php } else {  ?>
                                                          <img id="img{{$tmp->IdAlbum}}"  src="img/video.png"  width="165" height="160"/>
                                                          <a class="del_photo" id="hidImg{{$tmp->IdAlbum}}" onclick="hedeImg('{{$tmp->IdAlbum}}')">  <i class="glyphicon glyphicon-remove"></i></a>
                                                    <?php } ?>

                                                  </div>
                                               @endforeach
                            </div>
                    </div>


                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
<script>
  var del = "";
  var Coutdel=0;
  function hedeImg(Id) {
    var IdImg = "img"+Id;
    var hidImg = "hidImg"+Id;
    var Imgs = "images"+Id;
    del +=","+Id;
    Coutdel += 1;

    document.getElementById(IdImg).style.visibility = "hidden";
    document.getElementById(hidImg).style.visibility = "hidden";
    document.getElementById(Imgs).style.visibility = "hidden";
    // document.getElementById("bdShow").style.visibility = "hidden";
    document.getElementById("delId").value = del;
    document.getElementById("coutDel").value = Coutdel;

  }


</script>
@endsection
