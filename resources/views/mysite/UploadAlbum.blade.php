@extends('layouts.app')
@section('content')
<style>
div.img {
  display: inline-block;
  position: relative;
  margin: 5px;
  border: 1px solid #ccc;
  float: left;
  width: 167px;
}

div.video {
  display: inline-block;
  position: relative;
  margin: 5px;
  border: 1px solid #ccc;
  float: left;
  width: 167px;
}

div.desc {
    padding: 10px;
    text-align: right;
}
.del_video {
    display: inline-block;
    float:right;
	  margin:5px 5px 0 0;
    position: absolute;
    top: 0;
    right: 0;
}

.del_photo {
    display: inline-block;
    float:right;
	  margin:5px 5px 0 0;
    position: absolute;
    top: 0;
    right: 0;
}
</style>
<div class="container">
<div class="row">
      <div class="col-md-12 ">
          <div class="panel panel-default">
              <div class="panel-heading">
                <form action="{{url('Savemutiupload')}}" method="post">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <input type="hidden" value="{{$idAlbum}}" name="idAb" >
                      <input type="hidden" value="cancelPost" name="can" >
                    <button type="submit" class="close" onclick="return confirm('Cancel and Delete Photo?')">&times;</button>
                </form>
                Create Album

              </div>
              <div class="panel-body">
              <form action="{{url('Savemutiupload')}}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                <div class="row">
                  <div class="col-md-4 ">
                      <div class="form-group">
                                      <input type="text" class="form-control" id="inputname" name="NameAlbum" placeholder="Untited Album">
                                       <textarea class="form-control" placeholder="Say Somthing about this album..."  name="Description" rows="4" value="" style="width:100%; resize: none;"></textarea>
                                <hr>
                              <p class="help-block">Change Date</p>
                              <input type="date" name="bday">
                      </div>
                          <button type="submit" id="space" class="fl btn btn-primary btn-sm " > Post</button>

                  </div>

                    <div class="col-md-8 ">
                                              <input type="hidden" value="{{$idAlbum}}" name="idAb" >
                                              <input type="hidden"  name="delimg" id="delId">
                                              <input type="hidden"  name="coutDel" id="coutDel">
                                              <input type="hidden" value="save" name="can" >
                                             @foreach ($files as $tmp)
                                                <div class="img" id="images{{$tmp->IdAlbum}}">
                                                      <?php if($tmp->typefile =="image/png" ||$tmp->typefile =="image/jpeg"  ||$tmp->typefile =="image/gif") {?>
                                                        <img id="img{{$tmp->IdAlbum}}" src="{{URL::to('/') }}/{{$tmp->pathimg}}/{{ $tmp->nameimg}}"  width="165" height="160"/>
                                                        <a class="del_photo" id="hidImg{{$tmp->IdAlbum}}" onclick="hedeImg('{{$tmp->IdAlbum}}')">  <i class="glyphicon glyphicon-remove"></i></a>
                                                      <?php } else {  ?>
                                                        <video width="165" height="160" controls >
                                                          <source id="img{{$tmp->IdAlbum}}" src="{{URL::to('/') }}/{{$tmp->pathimg}}/{{$tmp->nameimg}}" type="{{$tmp->typefile}}" >
                                                          <a class="del_video" id="hidImg{{$tmp->IdAlbum}}" onclick="hedeImg('{{$tmp->IdAlbum}}')">  <i class="glyphicon glyphicon-remove"></i></a>
                                                        </video>
                                                      <?php } ?>
                                                </div>

                                             @endforeach
                                          </div>
                    </div>
                  </form>
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
<script>
  var del = "";
  var Coutdel=0;
  function hedeImg(Id) {
    var IdImg = "img"+Id;
    var hidImg = "hidImg"+Id;
    var Imgs = "images"+Id;
    del +=","+Id;
    Coutdel += 1;

    document.getElementById(IdImg).style.visibility = "hidden";
    document.getElementById(hidImg).style.visibility = "hidden";
    document.getElementById(Imgs).style.visibility = "hidden";
    document.getElementById("delId").value = del;
    document.getElementById("coutDel").value = Coutdel;

  }


</script>
@endsection
