<!DOCTYPE html>
<?php
use App\service;
?>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PL36</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style>

.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 250px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}


.modal-content {
    position: relative;
    background-color: #f5f5f5;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 35%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}


.modal-body {padding: 2px 16px;}
.modal-backdrop {
    top:auto;
    right:auto;
    bottom:auto;
    left:auto;
    background-color: #292929;
    position: fixed;
}
</style>

    <style>
    #search input[type="text"] {
        background: url(search-dark.png) no-repeat 10px 6px #444;
        border: 0 none;
        font: bold 12px Arial,Helvetica,Sans-serif;
        color: #777;
        width: 150px;
        padding: 6px 15px 6px 35px;
        -webkit-border-radius: 20px;
        -moz-border-radius: 20px;
        border-radius: 20px;
        text-shadow: 0 2px 2px rgba(0, 0, 0, 0.3);
        -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
        -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
        box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
        -webkit-transition: all 0.7s ease 0s;
        -moz-transition: all 0.7s ease 0s;
        -o-transition: all 0.7s ease 0s;
        transition: all 0.7s ease 0s;
    }
    #search input[type="text"]:focus {
      width: 200px;
    }
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
        .circleborder {
        width: 30px;
        height: 30px;
        border-radius: 150px;
        -webkit-border-radius: 150px;
        -moz-border-radius: 150px;
        background: url(URL) no-repeat;
        box-shadow: 0 0 8px rgba(0, 0, 0, .8);
        -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
        -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
}
    </style>

    <style>
    #images {
        display: inline-block;
        width: 100px;
        height: 100px;
        position: relative;
    }

    .del_photo {
        display: inline-block;
        float:right;
    	   margin:5px 5px 0 0;
        position: absolute;
        top: 0;
        right: 0;
    }

    </style>
    <style>
    div.img {
      display: inline-block;
      position: relative;
      margin: 7px;
      border: 1px solid #f9f8f8;
      float: left;
      width: 160px;
    }
    div.desc {
        padding: 10px;
        text-align: center;
    }
    </style>
    <!-- Tab -->
  <style>
  body {font-family: "Lato", sans-serif;}

  ul.tab {
      list-style-type: none;
      margin: 0;
      padding: 0;
      overflow: hidden;
      border: 0px solid #ccc;
      background-color: #f1f1f1;
  }
  /* Float the list items side by side */
  ul.tab li {float: left;}
  /* Style the links inside the list items */
  ul.tab li a {
      display: inline-block;
      color: black;
      text-align: center;
      padding: 8px 18px;
      text-decoration: none;
      transition: 0.1s;
      font-size: 20px;
  }
  /* Change background color of links on hover */
  ul.tab li a:hover {
      background-color: #ddd;
  }
  /* Create an active/current tablink class */
  ul.tab li a:focus, .active {
      background-color: #ccc;
  }
  /* Style the tab content */
  .tabcontent {
      display: none;
      padding: 6px 12px;
      -webkit-animation: fadeEffect 1s;
      animation: fadeEffect 1s;
  }
  @-webkit-keyframes fadeEffect {
      from {opacity: 0;}
      to {opacity: 1;}
  }
  @keyframes fadeEffect {
      from {opacity: 0;}
      to {opacity: 1;}
  }
  textarea {
    resize: none;
  }
  </style>
<!-- Tab -->

<!-- uploadfile -->
  <style>
  .js .inputfile {
      width: 0.1px;
      height: 0.1px;
      opacity: 0;
      overflow: hidden;
      position: absolute;
      z-index: -1;
  }
  .inputfile + label {
      max-width: 80%;
      font-size: 1.25rem;
      /* 20px */
      font-weight: 700;
      text-overflow: ellipsis;
      white-space: nowrap;
      cursor: pointer;
      display: inline-block;
      overflow: hidden;
      padding: 0.625rem 1.25rem;
      /* 10px 20px */
  }
  .no-js .inputfile + label {
      display: none;
  }
  .inputfile:focus + label,
  .inputfile.has-focus + label {
      outline: 1px dotted #000;
      outline: -webkit-focus-ring-color auto 5px;
  }
  /* style 4 */
  .inputfile-4:focus + label,
  .inputfile-4.has-focus + label,
  .inputfile-4 + label:hover {
      color: #722040;
  }
  .inputfile-4 + label figure {
      width: 100px;
      height: 100px;
      border-radius: 50%;
      background-color: #d3394c;
      display: block;
      padding: 20px;
      margin: 0 auto 10px;
  }
  .inputfile-4 + label svg {
      width: 100%;
      height: 100%;
      fill: #f1e5e6;
  }
  </style>
  <script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>

  <!-- uploadfile -->


</head>

<body id="app-layout" style="padding-top:0px;">
  <div  >
    <nav class="navbar navbar-inverse navbar-static-top navbar-fixed-top">
        <div class="container" >
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}" style="padding: 0px 0px 0px 5px;">
                    <img src="img/icon/pl36.png" width="50px"  >
                </a>
                @if (Auth::guest())
                    <!-- <li><a href="{{ url('/home') }}">Home</a></li> -->
                @else

                <ul class="nav navbar-nav" style=" padding: 8px 0px 0px 0px;">
                    <form method="get" action="/search" id="search">
                        &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;
                        <input    name="q" type="text" size="40" placeholder="Search..." />
                    </form>
                </ul>
                @endif
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    @if (Auth::guest())
                        <!-- <li><a href="{{ url('/home') }}">Home</a></li> -->
                    @else
                    <?php $DB = service::find(Auth::user()->id); ?>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                         @if( Auth::user()->path_img == null)
                        <li><a href="{{ url('/1') }}">
                               <img class="circleborder" src="img/texture/default profile picture.png" width="25px" height="25px">
                             </a>
                        </li>
                        @else
                        <li >
                            <a href="{{ url('/home') }}"  >
                              <div style="position: absolute; top: 7px;">
                               <img class="circleborder" src="{{ Auth::user()->path_img }}" width="30px" height="30px" >
                             </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ Auth::user()->fristname }}
                             </a>
                        </li>
                        <li ><a href="{{ url('/custommysite') }}"  >
                               &nbsp;&nbsp;<i class="glyphicon glyphicon-move"></i>
                             </a>
                        </li>
                        <?php if($DB->mysite==1){ ?>
                        <li ><a href="{{ url('/mysite') }}"  >
                               |&nbsp;&nbsp;<i class="glyphicon glyphicon-modal-window"></i>
                             </a>
                        </li>
                        <?php } ?>
                        <?php if($DB->myfile==1){ ?>
                        <li ><a href="{{ url('/myflie') }}"  >
                               <i class="glyphicon glyphicon-folder-open"></i>
                             </a>
                        </li>
                        <?php } ?>
                        <?php if($DB->myphoto==1){ ?>
                        <li ><a href="{{ url('/home') }}"  >
                               <i class="glyphicon glyphicon-picture"></i>&nbsp;&nbsp;|
                             </a>
                        </li>
                        <?php } ?>
                        @endif
                        <!-- เพื่อน -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="glyphicon glyphicon-user"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>Emtry</li>
                            </ul>
                        </li>
                        <!-- แจ้งเตือน -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="glyphicon glyphicon-globe"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>Emtry</li>
                            </ul>
                        </li>
                        <!-- ตั้งค่า -->
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="glyphicon glyphicon-cog"></i> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/changeimg') }}"><i class="fa fa-btn fa-user"></i>Edit Profile</a></li>
                                <li><a href="{{ url('/service') }}"><i class="fa fa-btn fa-cog"></i>Setting Service</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
  </div><br><br>

    @yield('content')
            <!-- JavaScripts -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
            {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

</body>
</html>

<!-- custom ------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<?php
use App\customprofile;
use App\custommysite;
$custom = customprofile::find(Auth::user()->id);
$customs = custommysite::find(Auth::user()->id);
$s = $customs->position;
$indextolist = $customs->indextolist;
?>
    <link rel="stylesheet" href="css/profile style.css">
    <link rel="stylesheet" href="dist/gridstack.css"/>
    <link rel="stylesheet" href="dist/gridstack-extra.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
    <script src="dist/gridstack.js"></script>
    <style type="text/css">
        .grid-stack {
            background: ;
        }

        .grid-stack-item-content {
            color: #333;
        }
    </style>
    <!-- slide bar -->
    <link href="dist/slide/thumbs2.css" rel="stylesheet" />
    <link href="dist/slide/thumbnail-slider.css" rel="stylesheet" />
    <script src="dist/slide/thumbnail-slider.js" type="text/javascript"></script>
<body style="background-color:rgba(106, 70, 25, 0.63);">
    <div class="device-xs visible-xs"></div>
    <div class="device-sm visible-sm"></div>
    <div class="device-md visible-md"></div>
    <div class="device-lg visible-lg"></div>
    <div class="device-xl visible-xl"></div>

    <div class="container">
        <div class=" panel panel-default">
          <div class="panel-body">
            <div class=" grid-stack">
            </div>
          </div>
        </div>
    </div>
    <div id="thumbs2" style="display:none;">
        <div class="inner">
            <ul>
            @if($idshow == 0)
            <?php
              for($i = 1 ; $i<=11; $i++){ ?>
                <li>
                    <a class="thumb" href="img/<?= $i ?>.jpg"></a>
               </li>
              <?php  }
              ?>
            @else
                @foreach($PostAlbum as $Post)
                    @if($Post->IdPostAlbums==$idshow)
                    @foreach ($objAlbum as $album)
                        <?php if ($Post->IdPostAlbums==$album->IdPostAlbums) { ?>
                          <li>
                            <?php if($album->typefile =="image/png" ||$album->typefile =="image/jpeg"  ||$album->typefile =="image/gif") {?>
                                    <a class="thumb"  href="{{URL::to('/') }}/{{$album->pathimg}}/{{ $album->nameimg}}"></a>
                              <?php } else {  ?>
                                  <video  width="100%" height="100%"  poster="img/video.png" controls >
                                    <source class="thumb" src="{{URL::to('/') }}/{{$album->pathimg}}/{{$album->nameimg}}" type="{{$album->typefile}}" >
                                  </video>
                            <?php } ?>
                          </li>
                        <?php } ?>
                    @endforeach
                    @endif
                @endforeach
            @endif
            </ul>
        </div>
        <div id="closeBtn">X</div>
      </div>


    <script type="text/javascript">

        $(function () {
            var i=0;
            var x1 =[];
            var y1 = [];
            var w1 = {};
            var h1 = {};
            var sqrt = {};
            var sqrtbuff = [];
            var width = {};
            // thanks to http://stackoverflow.com/a/22885503
            var waitForFinalEvent=function(){var b={};return function(c,d,a){a||(a="I am a banana!");b[a]&&clearTimeout(b[a]);b[a]=setTimeout(c,d)}}();
            var fullDateString = new Date();
            function isBreakpoint(alias) {
                return $('.device-' + alias).is(':visible');
            }


            var options = {
                float: false
            };
            $('.grid-stack').gridstack(options);
            function resizeGrid() {
                var grid = $('.grid-stack').data('gridstack');
                if (isBreakpoint('xs')) {
                    $('#grid-size').text('One column mode');
                } else if (isBreakpoint('sm')) {
                    grid.setGridWidth(3);
                    $('#grid-size').text(3);
                } else if (isBreakpoint('md')) {
                    grid.setGridWidth(6);
                    $('#grid-size').text(6);
                } else if (isBreakpoint('lg')) {
                    grid.setGridWidth(12);
                    $('#grid-size').text(12);
                }
            };
            $(window).resize(function () {
                waitForFinalEvent(function() {
                    resizeGrid();
                }, 300, fullDateString.getTime());
            });
            //-----------------------------------------------------slide show img----------------------------------------------------------------------------------------
            var slideshowimg ='<div class="center">'+
                                  "<div class='col-sm-12' >"+
                                      "<div id='thumbnail-slider' >"+
                                          "<div class='inner'>"+
                                              "<ul>"+
                                                "@if($idshow == 0)"+
                                                  <?php
                                                    for($i = 1 ; $i<=11; $i++){ ?>
                                                    "<li>"+
                                                      "<a class='thumb'  href='img/<?= $i ?>.jpg' onclick='hhh()' style='object-fit: cover;  width: 100%; height:100%;'></a>"+
                                                    "</li>"+
                                                    <?php }
                                                    ?>
                                                  "@else"+
                                                      "@foreach ($PostAlbum as $Post)"+
                                                              "@if($Post->IdPostAlbums==$idshow)"+
                                                                  "@foreach ($objAlbum as $album)"+
                                                                        "<?php if ($Post->IdPostAlbums==$album->IdPostAlbums) { ?>"+
                                                                          "<li>"+

                                                                              '<?php if($album->typefile =="image/png" ||$album->typefile =="image/jpeg"  ||$album->typefile =="image/gif") {?>'+
                                                                                  '<a class="thumb"  href="{{URL::to('/') }}/{{$album->pathimg}}/{{ $album->nameimg}}" onclick="hhh()" style="object-fit: cover;  width: 100%; height:100%;"></a>'+
                                                                              '<?php } else {  ?>'+
                                                                                  '<a class="thumb"  href="img/video.png" onclick="hhh()" style="object-fit: cover;  width: 100%; height:100%;"></a>'+
                                                                            '<?php } ?>'+
                                                                          "</li>"+
                                                                        "<?php } ?>"+
                                                                  "@endforeach"+
                                                              "@endif"+
                                                      "@endforeach"+
                                                  "@endif"+
                                              "</ul>"+
                                          "</div>"+
                                      "</div>"+
                                      '</div>'+
                                  "</div>";
              //-----------------------------------------------------profile----------------------------------------------------------------------------------------
              var profile =  "<center>"+
                                      "<img src='{{ Auth::user()->path_img }}'style='object-fit: cover;  width: 100%; height:100%;'>"+
                                      "<div class='centerbot'>"+
                                        "<div class='panel ' style='width:100%;background-color:hsla(0, 0%, 0%, 0);'>"+
                                          "{{Auth::user()->fristname}}&nbsp;&nbsp;&nbsp;{{Auth::user()->lastname}}({{Auth::user()->name}})"+
                                        "</div>"+
                                      "</div>"+
                              "</center>";
            //---------------------------------------------------------img cover---------------------------------------------------------------------------------------------

            var imgcover = "<img  src='{{ $custom->cover }}'style='object-fit: cover;  width: 100%; height:101%;'>";

            //---------------------------------------------------------youtube-----------------------------------------------------------------------------------

            var youtube = '<object width="100%" height="102%"data="http://www.youtube.com/embed/QyhrOruvT1c?autoplay=1">'+
                           '</object>';

            //---------------------------------------------------------Post us-----------------------------------------------------------------------------------

            var postview = "<div class='row'>"+
                                    "<div class='col-md-12' >"+
                                        "<div class='panel panel-default'>"+
                                                                "<ul class='tab'>"+
                                                                  '<li><a class="tablinks" id="preload" onclick="openTabPost(event, \'Status\')" >Status</a></li>'+
                                                                  '<li><a class="tablinks" onclick="openTabPost(event, \'Photo\')" >Photo/Video</a></li>'+
                                                                "</ul>"+
                                                      " <div class='panel-body'>"+
                                                        '<form action="{{$url}}" role="form" enctype="multipart/form-data" method="post" >'+
                                                              '<input type="hidden" name="iduser" value="{{ Auth::user()->id }}">'+
                                                              '<input type="hidden" name="idname" value="{{ Auth::user()->name }}">'+
                                                              '<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">'+

                                                              "<div id='Status' class='tabcontent'>"+
                                                                  "<div class='form-group{{ $errors->has('status') ? ' has-error' : '' }}'>"+
                                                                        "<textarea class='form-control' placeholder='What is on you mind?'  name='status' rows='5'  style='width:100%; resize: none;'></textarea>"+
                                                                          "@if ($errors->has('status'))"+
                                                                              "<span class='help-block'>"+
                                                                                "  <strong>This post appears to be blank. Please write something or attach a link or photo to post.</strong>"+
                                                                              "</span>"+
                                                                          "@endif"+
                                                                          "<hr>"+
                                                                    "</div>"+//end  form-group
                                                                    "<div class='form-group'>"+
                                                                    "<label for='inputEmail3' class='col-sm-2 control-label'>Photo/Video</label>"+
                                                                                "<div class='form-group{{ $errors->has('status') ? ' has-error' : '' }}'>"+
                                                                                      "<div class='col-sm-10 box'>"+
                                                                                        "<input type='file' name='file' id='file' class='inputfile inputfile-4'  accept='image/* ,video/*' />"+
                                                                                         "<label for='file'>"+
                                                                                              "<img  img src='img/upImg.png' alt='Smiley face' height='100%' width='100' class='img-rounded'>"+
                                                                                              "<br>"+
                                                                                              "<span>Choose image&hellip;</span>"+
                                                                                          "</label>"+
                                                                                          "@if ($errors->has('file'))"+
                                                                                              "<span class='help-block'>"+
                                                                                                "  <strong>You can only have up files image or video files only.</strong>"+
                                                                                              "</span>"+
                                                                                          "<hr>"+
                                                                                          "@endif"+
                                                                                      "</div>"+//end  col-sm-10 box
                                                                                "</div>"+//end  form-group $errors
                                                                      "</div>"+//end  form-group
                                                                      "<div class='form-group'>"+
                                                                       " <button type='submit' id='space' class='fl btn btn-primary btn-sm'  onclick='myFunction()' > Post</button>"+
                                                                      "</div>"+//end  form-group
                                                              "</div>"+//end  class='tabcontent'
                                                              "</form>"+//end form Status

                                                              '<form action="mutiupload" role="form" enctype="multipart/form-data" method="post" >'+


                                                                    '<input type="hidden" name="iduser" value="{{ Auth::user()->id }}">'+
                                                                    '<input type="hidden" name="idname" value="{{ Auth::user()->name }}">'+
                                                                    '<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">'+

                                                                    "<div id='Photo' class='tabcontent'>"+
                                                                          "<div class='form-group'>"+
                                                                          "<label for='inputEmail' class='col-sm-2 control-label'>Photo/Video</label>"+
                                                                                            "<div class='col-sm-10 box'>"+

                                                                                              "<input type='file' name='filemuti[]' id='file-5' class='inputfile inputfile-4'  accept='image/*  ,video/*' data-multiple-caption='{count} files selected' multiple />"+
                                                                                               "<label for='file-5'>"+
                                                                                                    "<img  img src='img/upImg.png' alt='Smiley face' height='100%' width='100' class='img-rounded'>"+
                                                                                                    "<br>"+
                                                                                                    "<span>Choose image&hellip;</span>"+
                                                                                                "</label>"+
                                                                                                "@if(Session::has('fail'))"+
                                                                                                    "<span class='help-block'>"+
                                                                                                      "<strong>{!! Session::get('fail') !!}</strong>"+
                                                                                                    "</span>"+
                                                                                                "<hr>"+
                                                                                                "@endif"+
                                                                                            "</div>"+//end  col-sm-10 box
                                                                            "</div>"+//end  form-group
                                                                            "<div class='form-group'>"+
                                                                             " <button type='submit' id='space' class='fl btn btn-primary btn-sm' > Create</button>"+
                                                                            "</div>"+//end  form-group
                                                                    "</div>"+//end  class='tabcontent'
                                                              "</form>"+//end form Album
                                                      "</div>"+//end body
                                        "</div>"+//end panel panel-default
                                    "</div>"+//end col-md-12

                                    "<div class='col-md-12' >"+
                                        "<div class='panel panel-default'>"+
                                                                "<ul class='tab'>"+
                                                                  '<li><a class="tablinks2" id="preload2" onclick="openTabShow(event, \'ShowStatus\')" >MyStatus</a></li>'+
                                                                  '<li><a class="tablinks2" onclick="openTabShow(event, \'ShowPhoto\')" >MyPhoto</a></li>'+
                                                                "</ul>"+
                                                      " <div class='panel-body'>"+
                                                              "<div id='ShowStatus' class='tabcontent2'>"+
                                                                  "@if (count($objs) == 0)"+
                                                                    '<center><p class="help-block">You have no status.</p></center>'+
                                                                  "@endif"+
                                                                  "@foreach ($objs as $key)"+
                                                                        "<h4>"+
                                                                        "<strong>"+
                                                                            '<a style="color:black;"  href="/home">{{ Auth::user()->name }} </a>'+
                                                                        "</strong>"+
                                                                        "<small>{{$key->created_at}}"+

                                                                        "</small></h4>"+
                                                                        '<div id="{{$key->IdStatus}}" class="{{$key->IdStatus}}"></div>'+
                                                                            "<div class='panel-body'>"+
                                                                                "<p>{{$key->status}}</p>"+
                                                                                  '@if($key->typefile !="")'+
                                                                                        '@if($key->typefile =="image/png" ||$key->typefile =="image/jpeg"  ||$key->typefile =="image/gif")'+
                                                                                              '<img class="img-rounded" width="100%" height="100%" src="{{URL::to('/') }}/{{$key->pathimg}}/{{ $key->nameimg}}"/>'+
                                                                                        '@else'+
                                                                                              '<video class="img-rounded" width="100%" height="100%"  poster="img/video.png" controls >'+
                                                                                                  '<source src="{{URL::to('/') }}/{{$key->pathimg}}/{{$key->nameimg}}" type="{{$key->typefile}}" >'+
                                                                                              '</video>'+
                                                                                        '@endif'+
                                                                                  '@endif'+

                                                                                  '<div id="hidstatus{{$key->IdStatus}}" align="right">'+
                                                                                    "<br>"+
                                                                                      "@if($key->typefile !="")"+
                                                                                          '@if($key->typefile =="image/png" ||$key->typefile =="image/jpeg"  ||$key->typefile =="image/gif")'+
                                                                                                  '<a id="space" onclick="myEditPost(\'{{$key->IdStatus}}\',\'{{$key->status}}\' ,\'{{URL::to('/') }}/{{$key->pathimg}}/{{ $key->nameimg}}\' )"> '+
                                                                                                      "<img  img src='img/edit.png' alt='Smiley face'  class='img-rounded'>"+
                                                                                                  '</a>'+
                                                                                                  '<a href="{{URL::to('mysite/del/')}}/{{$key->IdStatus}}" onclick="return confirm(\'are you sure?\')">'+
                                                                                                      "<img  img src='img/del.png' alt='Smiley face'  class='img-rounded'>"+
                                                                                                  '</a>'+
                                                                                          '@else'+
                                                                                                '<a id="space" onclick="myEditPost(\'{{$key->IdStatus}}\',\'{{$key->status}}\' ,\'img/delvideo.png\' )"> '+
                                                                                                    "<img  img src='img/edit.png' alt='Smiley face'  class='img-rounded'>"+
                                                                                                '</a>'+
                                                                                                    '<a href="{{URL::to('mysite/del/')}}/{{$key->IdStatus}}" onclick="return confirm(\'are you sure?\')">'+
                                                                                                        "<img  img src='img/del.png' alt='Smiley face'  class='img-rounded'>"+
                                                                                                    '</a>'+

                                                                                          '@endif'+


                                                                                      "@else"+
                                                                                          '<a id="space" onclick="myEditPost2(\'{{$key->IdStatus}}\',\'{{$key->status}}\' )"> '+
                                                                                              "<img  img src='img/edit.png' alt='Smiley face'  class='img-rounded'>"+
                                                                                          '</a>'+
                                                                                          '<a href="{{URL::to('mysite/del/')}}/{{$key->IdStatus}}" onclick="return confirm(\'are you sure?\')">'+
                                                                                              "<img  img src='img/del.png' alt='Smiley face'  class='img-rounded'>"+
                                                                                          '</a>'+
                                                                                      "@endif"+
                                                                                  "</div>"+//end align="right"

                                                                            "</div>"+//end panel-body
                                                                        "<hr>"+
                                                                  "@endforeach"+
                                                              "</div>"+//end  class='tabcontent'
                                                              "<div id='ShowPhoto' class='tabcontent2'>"+
                                                                    "@if (count($PostAlbum) == 0)"+
                                                                      '<center><p class="help-block">You have no Albums.</p></center>'+
                                                                    "@endif"+
                                                                    '<div align="center" class="panel-body">'+
                                                                          "@foreach ($PostAlbum as $Post)"+
                                                                                  "@if($Post->confrim==1)"+
                                                                                      "@foreach ($objAlbum as $album)"+
                                                                                            "<?php if ($Post->IdPostAlbums==$album->IdPostAlbums) { ?>"+
                                                                                                '<div class="img" >'+
                                                                                              '@if($album->typefile =="image/png" ||$album->typefile =="image/jpeg"  ||$album->typefile =="image/gif")'+
                                                                                                        '<a href="{{URL::to('setShow/')}}/{{$Post->IdPostAlbums}}/{{$Post->id}}" onclick="hhh()">'+
                                                                                                            '<img src="{{URL::to('/') }}/{{$album->pathimg}}/{{ $album->nameimg}}"  width="158" height="150"  class="w3-hover-opacity img-rounded" />'+
                                                                                                        "</a>"+
                                                                                              '@else'+
                                                                                                        '<a href="{{URL::to('setShow/')}}/{{$Post->IdPostAlbums}}/{{$Post->id}}" onclick="hhh()">'+
                                                                                                            '<img src="img/covervideo.png"  width="158" height="150"  class="w3-hover-opacity img-rounded" />'+
                                                                                                        "</a>"+
                                                                                              '@endif'+

                                                                                                      '<a  href="{{$Post->IdPostAlbums}}">'+
                                                                                                        '<div class="desc" style="color:black;" >{{$Post->nameAlbum}}</div>'+
                                                                                                      "</a>"+
                                                                                                "</div>"+//end  img
                                                                                            "<?php break; } ?>"+
                                                                                      "@endforeach"+
                                                                                  "@endif"+
                                                                          "@endforeach"+
                                                                    "</div>"+//end panel-body
                                                              "</div>"+//end  class='tabcontent'
                                                      "</div>"+//end panel-body
                                      "</div>"+//end panel panel-default
                                  "</div>"+//end col-md-12

                                "</div>";//end row


            //---------------------------------------------------------note-----------------------------------------------------------------------------------

            var note =  "<img src='img/imgshow/unnamed.png'style='object-fit: cover; width: 100%;'>"+
                        "<img src='img/imgshow/unnamed.png'style='object-fit: cover; width: 100%;'>"+
                        "<img src='img/imgshow/unnamed.png'style='object-fit: cover; width: 100%;'>";


            new function () {
                this.serializedData = <?= $s ?> ;// Grid layout
                var positionindex = <?= $indextolist ?>;
                var layout = [profile,imgcover,slideshowimg,youtube,postview,note];
                this.grid = $('.grid-stack').data('gridstack');
                this.loadGrid = function () {
                    this.grid.removeAll();
                    var items = GridStackUI.Utils.sort(this.serializedData);
                    _.each(items, function (node, i) {
                        this.grid.addWidget($('<div id="'+positionindex[i]+'"><div style="background-color:rgba(#ff8e00);" class="grid-stack-item-content panel panel-default" >'
                                                +layout[positionindex[i]]+
                                              '</div></div>'),
                            node.x, node.y, node.width, node.height);
                    }, this);
                    return false;
                }.bind(this);

                this.saveGrid = function () {
                  i =0;
                    this.serializedData = _.map($('.grid-stack > .grid-stack-item:visible'), function (el) {
                        el = $(el);
                        var node = el.data('_gridstack_node');
                        //---------------------------------------------------------------------------------------------------
                        var sqt = Math.sqrt((node.x*node.x)+(node.y*node.y)) ;
                        var w = (node.width*node.height);
                        //console.log(i +": "+"x "+node.x+" y "+node.y+" w "+node.width+" h "+node.height+"="+w+" "+ "sqrt :"+sqt);
                        console.log("----------------------------");
                        x1[positionindex[i]] = node.x;
                        y1[positionindex[i]] = node.y;
                        w1[positionindex[i]] = node.width;
                        h1[positionindex[i]] = node.height;
                        width[positionindex[i]] = w;
                        sqrt[positionindex[i]] = sqt;
                        sqrtbuff[positionindex[i]] = sqt;
                        //---------------------------------------------------------------------------------------------------
                        i++;
                        return {
                            x: node.x,
                            y: node.y,
                            width: node.width,
                            height: node.height
                        };
                    }, this);
                    //-----------------------------------------------------------------------------------------------------------
                      for(var j = 0 ;j<i;j++){
                        console.log("id"+j+":"+sqrtbuff[j]+" y:"+y1[j]+" x:"+x1[j]);
                      }
                      var maxy = -9999999;
                      for(var j = 0 ;j<i;j++){
                        if(maxy<y1[j])
                          maxy = y1[j];
                      }
                      var maxx = -9999999;
                      for(var j = 0 ;j<i;j++){
                        if(maxx<x1[j])
                          maxx = x1[j];
                      }
                      var x2;
                      var y2;
                      var tmp;
                      var s = [];
                      for(var j = 0 ;j<i;j++){
                             x2 = x1[j]+"";
                             y2 = y1[j]+"";
                             if(y2.length<2){
                             tmp = y2;
                             y2 = "0"+tmp;
                             }
                             if(x2.length<2){
                            tmp = x2;
                            x2 = "0"+tmp;
                             }
                             console.log(y2+":"+x2+"f");
                             s[j] = y2+""+x2+""+j;
                      }
                      s.sort();
                      console.log('--------------------------');
                      for(var j = 0 ;j<i;j++){
                             console.log(s[j]);
                             console.log(s[j].charAt((s[j].length)-1));
                      }
                      var indextolist = "["+s[0].charAt((s[0].length)-1)+','+
                                        s[1].charAt((s[1].length)-1)+','+
                                        s[2].charAt((s[2].length)-1)+','+
                                        s[3].charAt((s[3].length)-1)+','+
                                        s[4].charAt((s[4].length)-1)+','+
                                        s[5].charAt((s[5].length)-1)+"]";
                      document.getElementById("indextolist").value = indextolist;
                      console.log(indextolist);
                      console.log(maxy);
                      console.log(maxx);
                      document.getElementById("submit").disabled = false;
                    //---------------------------------------------------------------------------------------------------------
                    $('#saved-data').val(JSON.stringify(this.serializedData, null, '    '));
                    return false;
                }.bind(this);
                $('#load-grid').click(this.loadGrid);
                $('#clear-grid').click(this.clearGrid);
                $('#save-grid').click(this.saveGrid);
                this.loadGrid();
                resizeGrid();
            };
        });
    </script>
    </script>
    <!-- <script>
        document.getElementById("submit").disabled = true;
    </script> -->

</body>
<script>

    //Note: this script should be placed at the bottom of the page, or after the slider markup. It cannot be placed in the head section of the page.
    function hhh() {
      var thumbs1 = document.getElementById("thumbnail-slider");
      var thumbs2 = document.getElementById("thumbs2");
      var closeBtn = document.getElementById("closeBtn");
      var slides = thumbs1.getElementsByTagName("li");
      for (var i = 0; i < slides.length; i++) {
          slides[i].index = i;
          slides[i].onclick = function (e) {

              var li = this;
              var clickedEnlargeBtn = false;
              if (e.offsetX > 220 && e.offsetY < 25) clickedEnlargeBtn = true;
              if (li.className.indexOf("active") != -1 || clickedEnlargeBtn) {
                  thumbs2.style.display = "block";
                  mcThumbs2.init(li.index);
              }
          };
      }             // The function returns the product of p1 and p2
      thumbs2.onclick = closeBtn.onclick = function (e) {
          thumbs2.style.display = "none";
      };
}
</script>
<script>
window.onload = function(){
     document.getElementById('preload').click();
     document.getElementById('preload2').click();

}

function openTabPost(evt, tab) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tab).style.display = "block";
    evt.currentTarget.className += " active";
}

function openTabShow(evt, tab) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent2");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks2");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tab).style.display = "block";
    evt.currentTarget.className += " active";
}

function myEditPost(IdPost,status ,linkimg) {
      var id = IdPost;
      var Status = status;
      var im = linkimg;
      var url = "mysite/editPost";
      var taghtml ="<form action=\""+url+" \" method=\"post\">"+
                      "<div class='panel panel-default'>"+
                      '<div class="panel-heading">Edit Post</div>'+
                        "<div class='panel-body'>"+
                                "<div class=\"form-group{{ $errors->has('edit') ? ' has-error' : '' }}\">"+
                                    "<textarea class=\"form-control\" placeholder=\"What's on you mind?\"  rows=\"5\" resize: none;\""+" name="+"edit"+">"+status+"</textarea>"+
                                    "@if ($errors->has('edit'))"+
                                    "<span class=\"help-block\">"+
                                    "<strong>This post appears to be blank. Please write something or attach a link or photo to post.</strong>"+
                                    "</span>"+
                                    "@endif"+
                                "</div>"+
                                  "<input type=\"hidden\" name=\"_token\" value=\"<?php echo csrf_token(); ?>\" >"+
                                  "<input type=\"hidden\" name=\"idpost\" value=\""+id+"\" >"+
                                  "<input type=\"hidden\"  name=\"delimg\" id=\"delId\">"+
                                  "<br>"+
                                  "<hr>"+
                                "<div id='images'>"+
                                    "<img id=\"img"+id+"\" src=\""+im+"\" height=\"100\" width=\"100\" class=\"img-rounded\"> "+
                                    "<a class=\"del_photo\" id=\"hidImg"+id+"\" onclick=\"hedeImg('"+id+"')\"> "+
                                    "<i class=\"glyphicon glyphicon-remove\">"+
                                    "</i>"+
                                    "</a>"+
                                "</div>"+
                                "<div class=\"col-md-12\" align='right'>"+
                                    "<hr>"+
                                    "<br>"+
                                    "<input class=\"btn btn-primary btn-sm \" type = \"submit\" value=\"Save\" id=\"space\" >"+
                                    "<a onclick=\"return confirm('Discard Changes')\" class=\"btn btn-danger btn-sm\" id=\"space\"  href= \"{{ url('/mysite')}}\" >Cancel</a>"+
                                "</div>"+
                        "</div>"+
                      "</div>"+
                  "</form> ";
     document.getElementById(id).innerHTML = taghtml;
     @foreach ($objs as $key)
         var di = "hidstatus"+{{$key->IdStatus}};
         document.getElementById(di).style.display = "none";
     @endforeach
}

function hedeImg(Id) {
  var IdImg = "img"+Id;
  var hidImg = "hidImg"+Id;
  var del = ""+Id;
  //console.log(del);
  document.getElementById(IdImg).style.visibility = "hidden";
  document.getElementById(hidImg).style.visibility = "hidden";
  document.getElementById("delId").value = del;
}
function myEditPost2(IdPost,status) {
      var id = IdPost;
      var Status = status;
      var url = "mysite/editPost";
         var taghtml ="<form action=\""+url+" \" method=\"post\">"+
                        "<div class='panel panel-default'>"+
                        '<div class="panel-heading">Edit Post</div>'+
                          "<div class='panel-body'>"+
                               "<div class=\"form-group{{ $errors->has('edit') ? ' has-error' : '' }}\">"+
                                 "<textarea class=\"form-control\" placeholder=\"What's on you mind?\"  rows=\"5\" resize: none;\""+" name="+"edit"+">"+status+"</textarea>"+

                                 "@if ($errors->has('edit'))"+
                                   "<span class=\"help-block\">"+
                                   "<strong>This post appears to be blank. Please write something or attach a link or photo to post.</strong>"+
                                   "</span>"+
                                 "@endif"+
                                 "</div>"+
                                       "<input type=\"hidden\" name=\"_token\" value=\"<?php echo csrf_token(); ?>\" >"+
                                       "<input type=\"hidden\" name=\"idpost\" value=\""+id+"\" >"+
                                  "<div class=\"col-md-12\" align='right'>"+
                                    "<hr>"+
                                    "<br>"+
                                        "<input class=\"btn btn-primary btn-sm \" type = \"submit\" value=\"Save\" id=\"space\" >"+
                                        "<a onclick=\"return confirm('Discard Changes')\" class=\"btn btn-danger btn-sm\" id=\"space\" href= \"{{ url('/mysite')}}\" >Cancel</a>"+
                                  "</div>"+
                            "</div>"+
                          "</div>"+
                      "</form> ";
     document.getElementById(id).innerHTML = taghtml;
    @foreach ($objs as $key)
        var di = "hidstatus"+{{$key->IdStatus}};
        document.getElementById(di).style.display = "none";
    @endforeach

}

function hedeImg(Id) {
  var IdImg = "img"+Id;
  var hidImg = "hidImg"+Id;
  var del = ""+Id;
  //console.log(del);
  document.getElementById(IdImg).style.visibility = "hidden";
  document.getElementById(hidImg).style.visibility = "hidden";
  document.getElementById("delId").value = del;
}

</script>


<!-- uploadfile -->
<script>
;( function ( document, window, index )
{
  var inputs = document.querySelectorAll( '.inputfile' );
  Array.prototype.forEach.call( inputs, function( input )
  {
    var label	 = input.nextElementSibling,
      labelVal = label.innerHTML;
      console.log(label);
    input.addEventListener( 'change', function( e )
    {

      var fileName = '';
        console.log(fileName);
      if( this.files && this.files.length > 1 )
        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
      else
        fileName = e.target.value.split( '\\' ).pop();

      if( fileName )
        label.querySelector( 'span' ).innerHTML = fileName;
      else
        label.innerHTML = labelVal;
    });

    // Firefox bug fix
    input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
    input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
  });
}( document, window, 0 ));
</script>
<!-- uploadfile -->

<!-- POPUP @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="panel-heading">
      <h1><center><B>Error Edit Status.</B></center></h1>
      <hr>
   </div>
    <div class="modal-body">
     <p class="help-block"><h3><center>This post appears to be blank. </center></h3></p>
     <br>
    </div>

  </div>

</div>
<script type="text/javascript">
$(window).load(function()
{
    @if(Session::has('test'))
		$('#myModal').modal('show');
    @endif
});
</script>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myModal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}


</script>

<!-- <script>
$(function(){
var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");
	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 1);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});

$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});
$(window).resize(function() {
    $(".modal-box").css({
        top: (300) ,
        left: (450)
    });
});
$(window).resize();

});

</script> -->

<!-- POPUP @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
