<!DOCTYPE html>
<?php
use App\service;
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PL36</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style>
    #search input[type="text"] {
        background: url(search-dark.png) no-repeat 10px 6px #444;
        border: 0 none;
        font: bold 12px Arial,Helvetica,Sans-serif;
        color: #777;
        width: 150px;
        padding: 6px 15px 6px 35px;
        -webkit-border-radius: 20px;
        -moz-border-radius: 20px;
        border-radius: 20px;
        text-shadow: 0 2px 2px rgba(0, 0, 0, 0.3);
        -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
        -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
        box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1), 0 1px 3px rgba(0, 0, 0, 0.2) inset;
        -webkit-transition: all 0.7s ease 0s;
        -moz-transition: all 0.7s ease 0s;
        -o-transition: all 0.7s ease 0s;
        transition: all 0.7s ease 0s;
    }
    #search input[type="text"]:focus {
      width: 200px;
    }
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
        .circleborder {
        width: 30px;
        height: 30px;
        border-radius: 150px;
        -webkit-border-radius: 150px;
        -moz-border-radius: 150px;
        background: url(URL) no-repeat;
        box-shadow: 0 0 8px rgba(0, 0, 0, .8);
        -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
        -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
}
    </style>
</head>

<body id="app-layout" style="padding-top:0px;">
  <div  >
    <nav class="navbar navbar-inverse navbar-static-top navbar-fixed-top">
        <div class="container" >
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}" style="padding: 0px 0px 0px 5px;">
                    <img src="img/icon/pl36.png" width="50px"  >
                </a>
                @if (Auth::guest())
                    <!-- <li><a href="{{ url('/home') }}">Home</a></li> -->
                @else
                <ul class="nav navbar-nav" style=" padding: 8px 0px 0px 0px;">
                    <form method="get" action="/search" id="search">
                        &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;
                        <input    name="q" type="text" size="40" placeholder="Search..." />
                    </form>
                </ul>
                @endif
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    @if (Auth::guest())
                        <!-- <li><a href="{{ url('/home') }}">Home</a></li> -->
                    @else
                    <?php $DB = service::find(Auth::user()->id); ?>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                         @if( Auth::user()->path_img == null)
                        <li><a href="{{ url('/1') }}">
                               <img class="circleborder" src="img/texture/default profile picture.png" width="25px" height="25px">
                             </a>
                        </li>
                        @else
                        <li >
                            <a href="{{ url('/home') }}"  >
                              <div style="position: absolute; top: 7px;">
                               <img class="circleborder" src="{{ Auth::user()->path_img }}" width="30px" height="30px" >
                             </div>
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ Auth::user()->fristname }}
                             </a>
                        </li>
                        <li ><a href="{{ url('/mysite') }}"  >
                               |&nbsp;&nbsp;<i class="glyphicon glyphicon-arrow-left"></i>
                             </a>
                        </li>
                        <li ><a href="{{ url('/resetmysite') }}"  >
                               <i class="glyphicon glyphicon-repeat"></i>
                             </a>
                        </li>
                        <?php if($DB->mysite==1){ ?>
                        <li ><a href="{{ url('/mysite') }}"  >
                               |&nbsp;&nbsp;<i class="glyphicon glyphicon-modal-window"></i>
                             </a>
                        </li>
                        <?php } ?>
                        <?php if($DB->myfile==1){ ?>
                        <li ><a href="{{ url('/home') }}"  >
                               <i class="glyphicon glyphicon-folder-open"></i>
                             </a>
                        </li>
                        <?php } ?>
                        <?php if($DB->myphoto==1){ ?>
                        <li ><a href="{{ url('/home') }}"  >
                               <i class="glyphicon glyphicon-picture"></i>&nbsp;&nbsp;|
                             </a>
                        </li>
                        <?php } ?>
                        @endif
                        <!-- เพื่อน -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="glyphicon glyphicon-user"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>Emtry</li>
                            </ul>
                        </li>
                        <!-- แจ้งเตือน -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="glyphicon glyphicon-globe"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>Emtry</li>
                            </ul>
                        </li>
                        <!-- ตั้งค่า -->
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="glyphicon glyphicon-cog"></i> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/changeimg') }}"><i class="fa fa-btn fa-user"></i>Edit Profile</a></li>
                                <li><a href="{{ url('/service') }}"><i class="fa fa-btn fa-cog"></i>Setting Service</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
  </div><br><br>
    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>

<!-- custom ------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
<?php
use App\customprofile;
use App\custommysite;
$custom = customprofile::find(Auth::user()->id);
$customs = custommysite::find(Auth::user()->id);
$s = $customs->position;
$indextolist = $customs->indextolist;
?>
<script>$('#widget').draggable();</script>
    <link rel="stylesheet" href="css/profile style.css">
    <link rel="stylesheet" href="dist/gridstack.css"/>
    <link rel="stylesheet" href="dist/gridstack-extra.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
    <script src="dist/gridstack.js"></script>
     <!-- if previwe don't import -->
    <script src="dist/gridstack.jQueryUI.js"></script>
    <style type="text/css">
        .grid-stack {
            background: ;
        }

        .grid-stack-item-content {
            color: #fff;
        }
    </style>
    <!-- slide bar -->
    <link href="dist/slide/thumbs2.css" rel="stylesheet" />
    <link href="dist/slide/thumbnail-slider.css" rel="stylesheet" />
    <script src="dist/slide/thumbnail-slider.js" type="text/javascript"></script>

<body>
    <div class="device-xs visible-xs"></div>
    <div class="device-sm visible-sm"></div>
    <div class="device-md visible-md"></div>
    <div class="device-lg visible-lg"></div>
    <div class="device-xl visible-xl"></div>

    <div class="container">
        <div class=" panel panel-default">
          <div class="panel-body">
            <div class=" grid-stack">
            </div>
          </div>
        </div>
    </div>
    <!-- tool edit mysite -->
    <div class=" panel panel-default draggable" style="position:fixed;left:40%;top:75%;" style="width:100%;" id="drag-1">
      <div class="panel-body" >
        <center>
          <form action="{{ url('/savecustommysite') }}" method="post">
            <input type="hidden" value="{{csrf_token() }}" name="_token">
            <textarea  name="position" id="saved-data" style="width:100%;"cols="20" rows="5" readonly="readonly"></textarea>
            position<input type="text" class="form-control" name="indextolist" id="indextolist" style="width:100px;"><br>
            <a class="btn btn-danger" id="save-grid" href="#" onclick="save()"><i class="glyphicon glyphicon-list-alt"></i> Getnerrate Grid</a>
            <button type="submit" class="btn btn-success" id="submit" href="#" ><i class="glyphicon glyphicon-floppy-save"></i> Save Grid</button>
          </form>
        </center>
      </div>
    </div>

    <!-- show img -->
    <div id="thumbs2" style="display:none;">
        <div class="inner">
            <ul>
            <?php
              for($i = 1 ; $i<=11; $i++){ ?>
                <li>
                    <a class="thumb" href="img/<?= $i ?>.jpg"></a>
               </li>
              <?php  }
              ?>
            </ul>
        </div>
        <div id="closeBtn">X</div>
      </div>

    <script type="text/javascript">

        $(function () {
            var i=0;
            var x1 =[];
            var y1 = [];
            var w1 = {};
            var h1 = {};
            var sqrt = {};
            var sqrtbuff = [];
            var width = {};
            // thanks to http://stackoverflow.com/a/22885503
            var waitForFinalEvent=function(){var b={};return function(c,d,a){a||(a="I am a banana!");b[a]&&clearTimeout(b[a]);b[a]=setTimeout(c,d)}}();
            var fullDateString = new Date();
            function isBreakpoint(alias) {
                return $('.device-' + alias).is(':visible');
            }


            var options = {
                float: false
            };
            $('.grid-stack').gridstack(options);
            function resizeGrid() {
                var grid = $('.grid-stack').data('gridstack');
                if (isBreakpoint('xs')) {
                    $('#grid-size').text('One column mode');
                } else if (isBreakpoint('sm')) {
                    grid.setGridWidth(3);
                    $('#grid-size').text(3);
                } else if (isBreakpoint('md')) {
                    grid.setGridWidth(6);
                    $('#grid-size').text(6);
                } else if (isBreakpoint('lg')) {
                    grid.setGridWidth(12);
                    $('#grid-size').text(12);
                }
            };
            $(window).resize(function () {
                waitForFinalEvent(function() {
                    resizeGrid();
                }, 300, fullDateString.getTime());
            });
            //-----------------------------------------------------slide show img----------------------------------------------------------------------------------------
            var slideshowimg = "<img  src='img/layout/slide.png'style='object-fit: cover;  width: 100%; height:101%;'>";
              //-----------------------------------------------------profile----------------------------------------------------------------------------------------
              var profile =  "<center>"+
                                      "<img class='img-thumbnail' src='{{ Auth::user()->path_img }}'style='object-fit: cover;  width: 100%; height:100%;'>"+
                                      "<div class='centerbot'>"+
                                        "<div class='panel ' style='width:100%;background-color:hsla(0,0%,0%,0.8);'>"+
                                          "{{Auth::user()->fristname}}&nbsp;&nbsp;&nbsp;{{Auth::user()->lastname}}({{Auth::user()->name}})"+
                                        "</div>"+
                                      "</div>"+
                              "</center>";
            //---------------------------------------------------------img cover---------------------------------------------------------------------------------------------

            var imgcover = "<img  src='{{ $custom->cover }}'style='object-fit: cover;  width: 100%; height:101%;'>";

            //---------------------------------------------------------youtube-----------------------------------------------------------------------------------

            var youtube = "<img  src='img/layout/youtube.png'style='object-fit: cover;  width: 100%; height:101%;'>";

            //---------------------------------------------------------Post us-----------------------------------------------------------------------------------

            var postview = "<img  src='img/layout/post.png'style='object-fit: cover;  width: 100%; height:100%;'>";

            //---------------------------------------------------------note-----------------------------------------------------------------------------------

            var note =  "<img  src='img/layout/note.png'style='object-fit: cover;  width: 100%; height:100%;'>";


            new function () {
                this.serializedData = <?= $s ?> ;// Grid layout
                var positionindex = <?= $indextolist ?>;
                var layout = [profile,imgcover,slideshowimg,youtube,postview,note];
                this.grid = $('.grid-stack').data('gridstack');
                this.loadGrid = function () {
                    this.grid.removeAll();
                    var items = GridStackUI.Utils.sort(this.serializedData);
                    _.each(items, function (node, i) {
                        this.grid.addWidget($('<div id="'+positionindex[i]+'"><div  class="grid-stack-item-content panel panel-default" >'
                                                +layout[positionindex[i]]+
                                              '</div></div>'),
                            node.x, node.y, node.width, node.height);
                    }, this);
                    return false;
                }.bind(this);

                this.saveGrid = function () {
                  i =0;
                    this.serializedData = _.map($('.grid-stack > .grid-stack-item:visible'), function (el) {
                        el = $(el);
                        var node = el.data('_gridstack_node');
                        //---------------------------------------------------------------------------------------------------
                        var sqt = Math.sqrt((node.x*node.x)+(node.y*node.y)) ;
                        var w = (node.width*node.height);
                        //console.log(i +": "+"x "+node.x+" y "+node.y+" w "+node.width+" h "+node.height+"="+w+" "+ "sqrt :"+sqt);
                        console.log("----------------------------");
                        x1[positionindex[i]] = node.x;
                        y1[positionindex[i]] = node.y;
                        w1[positionindex[i]] = node.width;
                        h1[positionindex[i]] = node.height;
                        width[positionindex[i]] = w;
                        sqrt[positionindex[i]] = sqt;
                        sqrtbuff[positionindex[i]] = sqt;
                        //---------------------------------------------------------------------------------------------------
                        i++;
                        return {
                            x: node.x,
                            y: node.y,
                            width: node.width,
                            height: node.height
                        };
                    }, this);
                    //-----------------------------------------------------------------------------------------------------------
                      for(var j = 0 ;j<i;j++){
                        console.log("id"+j+":"+sqrtbuff[j]+" y:"+y1[j]+" x:"+x1[j]);
                      }
                      var maxy = -9999999;
                      for(var j = 0 ;j<i;j++){
                        if(maxy<y1[j])
                          maxy = y1[j];
                      }
                      var maxx = -9999999;
                      for(var j = 0 ;j<i;j++){
                        if(maxx<x1[j])
                          maxx = x1[j];
                      }
                      var x2;
                      var y2;
                      var tmp;
                      var s = [];
                      for(var j = 0 ;j<i;j++){
                             x2 = x1[j]+"";
                             y2 = y1[j]+"";
                             if(y2.length<2){
                             tmp = y2;
                             y2 = "0"+tmp;
                             }
                             if(x2.length<2){
                            tmp = x2;
                            x2 = "0"+tmp;
                             }
                             console.log(y2+":"+x2+"f");
                             s[j] = y2+""+x2+""+j;
                      }
                      s.sort();
                      console.log('--------------------------');
                      for(var j = 0 ;j<i;j++){
                             console.log(s[j]);
                             console.log(s[j].charAt((s[j].length)-1));
                      }
                      var indextolist = "["+s[0].charAt((s[0].length)-1)+','+
                                        s[1].charAt((s[1].length)-1)+','+
                                        s[2].charAt((s[2].length)-1)+','+
                                        s[3].charAt((s[3].length)-1)+','+
                                        s[4].charAt((s[4].length)-1)+','+
                                        s[5].charAt((s[5].length)-1)+"]";
                      document.getElementById("indextolist").value = indextolist;
                      console.log(indextolist);
                      console.log(maxy);
                      console.log(maxx);
                      document.getElementById("submit").disabled = false;
                    //---------------------------------------------------------------------------------------------------------
                    $('#saved-data').val(JSON.stringify(this.serializedData, null, '    '));
                    return false;
                }.bind(this);
                $('#load-grid').click(this.loadGrid);
                $('#clear-grid').click(this.clearGrid);
                $('#save-grid').click(this.saveGrid);
                this.loadGrid();
                resizeGrid();
            };
        });
    </script>
    </script>
    <script>
        document.getElementById("submit").disabled = true;
    </script>

</body>
<script>
    //Note: this script should be placed at the bottom of the page, or after the slider markup. It cannot be placed in the head section of the page.
    function hhh() {
      var thumbs1 = document.getElementById("thumbnail-slider");
      var thumbs2 = document.getElementById("thumbs2");
      var closeBtn = document.getElementById("closeBtn");
      var slides = thumbs1.getElementsByTagName("li");
      for (var i = 0; i < slides.length; i++) {
          slides[i].index = i;
          slides[i].onclick = function (e) {

              var li = this;
              var clickedEnlargeBtn = false;
              if (e.offsetX > 220 && e.offsetY < 25) clickedEnlargeBtn = true;
              if (li.className.indexOf("active") != -1 || clickedEnlargeBtn) {
                  thumbs2.style.display = "block";
                  mcThumbs2.init(li.index);
              }
          };
      }             // The function returns the product of p1 and p2
      thumbs2.onclick = closeBtn.onclick = function (e) {
          thumbs2.style.display = "none";
      };
}
</script>
