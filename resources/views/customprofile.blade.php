@extends('layouts.app')<br>
@section('content')
<!doctype html>
<link rel="stylesheet" type="text/css" href="css/time.css">
<!-- tool color -->
    <link rel="stylesheet" type="text/css" href="spectrum.css">
    <script type="text/javascript" src="docs/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="spectrum.js"></script>
    <script type='text/javascript' src='docs/toc.js'></script>
    <script type='text/javascript' src='docs/docs.js'></script>
<!-- tool color -->
<script src="jscolor.js"></script>
<!-- popup -->
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href="http://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/profile style.css">
<link rel="stylesheet" href="css/popup.css">

<body background ="{{ $custom->bg }}" >
<!-- popup tool  color -->
  <!-- สีเส้นขอบ 1 -->
  <div id="popup1" class="modal-box" >
      <a href="#" class="js-modal-close close">×</a><br>
      <form action="{{url('/show')}}" method="post">
          <div class="panel-body form-group">
            <center>เปลียนสีเส้น <B>กรอบนอก</B></center>
            <hr><br>
            <center>
            <input type="hidden" value="{{csrf_token() }}" name="_token">
            <div id="border" class="panel panel-default" style="width:200px; height:200px; background-color:{{ $custom->linecolor2 }};border-color:{{ $custom->linecolor }};"></div><br>
            <input  id="c1" class="form-control input-lg" value="000000" style="width:100px;" name="colorborder"><br>
            <button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'} btn btn-info">
              Pick color
            </button><br>
            <input type="hidden" name="check" value="1">
            </center>
          </div>
          <div class="panel-body" align="right">
            <hr>
            <input class="btn btn-info" type="submit"  value="ยืนยัน" >
          </div>
    </form>

  </div>
  <!-- สีเส้นขอบ 2 -->
  <div id="popup2" class="modal-box" >
      <a href="#" class="js-modal-close close">×</a><br>
      <form action="{{url('/show')}}" method="post">
          <div class="panel-body form-group">
            <center>เปลียนสีเส้น <B>กรอบใน</B></center>
            <hr><br>
            <center>
            <input type="hidden" value="{{csrf_token() }}" name="_token">
            <div id="bg" class="panel panel-default" style="width:200px; height:200px; background-color:{{ $custom->linecolor2 }};border-color:{{ $custom->linecolor }};"></div><br>
            <input  id="c2" class="form-control input-lg" value="000000" style="width:100px;" name="colorbg"><br>
            <button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'} btn btn-info">
              Pick color
            </button><br>
            <input type="hidden" name="check" value="2">
            </center>
          </div>
          <div class="panel-body" align="right">
            <hr>
            <input class="btn btn-info" type="submit"  value="ยืนยัน" >
          </div>
    </form>
  </div>
  <script>
  function setTextColor(picker) {
    console.log('#' + picker.toString());
    document.getElementById('bg').style.backgroundColor = '#' + picker.toString()
    document.getElementById('border').style.borderColor = '#' + picker.toString()
    document.getElementById('c1').value = '#' + picker.toString()
    document.getElementById('c2').value = '#' + picker.toString()
  }
  </script>
<!-- popup tool  -->

<div class="container">
    <div class="row">
      <!-- โปรไฟล์ -->
      <div class="col-md-12" >

          <div class="panel panel-default" style="background-color:{{ $custom->linecolor2 }};border-color:{{ $custom->linecolor }};">
            <div class="topleft">
              <a class="buttonedit " data-modal-id="popup1"><i class="fa fa-btn fa-pencil-square-o"></i></a>
            </div>
            <div class="topleft2">
              <a class="buttonedit " data-modal-id="popup2"><i class="fa fa-btn fa-pencil-square-o"></i></a>
            </div>
              <!-- <div class="panel-heading">Dashboard</div> -->
              <div class="panel-body">
                <!-- รูปหน้าปก -->
                  <img src="{{ $custom->cover }}"  style="object-fit: cover;  width: 100%; height:300px;">
                <!-- รูปหน้าปก -->
                <!-- ปุ่มแก้ไขรูปหน้าปกขวาบน -->
                  <div class="topright" >
                    <a class="buttonedit" style="opacity:0.5;" href="{{ url('/customcover')}}"><i class="fa fa-btn fa-pencil-square-o"></i></a>
                  </div>
                <!-- ปุ่มแก้ไขขวาบน -->
                <div class="bottomleft">
                  <a class="buttonedit2" style="opacity:0.5;"  href="{{ url('/custombg')}}"><i class="fa fa-btn fa-pencil-square-o"></i>Background </a>
                  <a class="buttonedit2" style="opacity:0.5;"   href="{{ url('/customtexture')}}"><i class="fa fa-btn fa-pencil-square-o"></i>Texture </a>
                </div>
                <!-- รูปโปรไฟล์ เเละ ชื่อ-->
                  <div class="center">
                    <!-- รูปโปรไฟล์-->
                      <img class="circleborder" id="img"  src="{{Auth::user()->path_img}}" style=" width:150px; height: 150px;">
                    <!-- ปุ่มแก้ไขรูปโปรไฟล์-->
                      <div class="center"><br>
                         <a class="buttonedit" style="opacity:0.5;" href="{{ url('/changeimg') }}">
                           <i class="fa fa-btn fa-pencil-square-o"></i>
                         </a>
                      </div>
                    <br><br>
                    <!--ชื่อ-->
                    <div class="panel panel-default" style="width:100%;background-color:hsla(100,100%,100%,0.5);">
                      <center>{{Auth::user()->fristname}}&nbsp;&nbsp;&nbsp;{{Auth::user()->lastname}}<br>({{Auth::user()->name}})</center>
                    </div>
                  </div>
                <!-- รูปโปรไฟล์ เเละ ชื่อ-->
              </div>
          </div>
      </div>
      <!-- โปรไฟล์ -->
      <!-- popup custom profile -->
        <div class="col-md-6 " >
          <div class="panel panel-default" style="background-color:{{ $custom->linecolor2 }};border-color:{{ $custom->linecolor }};" style="background-image: url('{{ $custom->texture }}');">
                <!-- <div class="panel-heading">Dashboard</div> -->
                <div class="panel-body " >
                      <div class="col-md-12" style="background-image: url('{{ $custom->texture }}');">
                        <div class="panel-body">
                          <center>
                            <div class="panel panel-default" style="background-color:hsla(100,100%,100%,0.3);">
                              <h4>My Profile</h4>
                            </div>
                            </center>
                          <hr>
                          <div class="col-md-10 col-md-offset-1" >
                          <div class="panel panel-default" style="width:100%;background-color:hsla(100,100%,100%,0.4);">
                          <center>Gender</center>
                          <?php if(Auth::user()->gender == 'male'){ ?>
                            <center>
                              <img src="img\texture\male.png" width="150px">
                            </center>
                          <?php } ?>
                          <?php if(Auth::user()->gender == 'female'){ ?>
                            <center>
                              <img src="img\texture\female.png" width="150px">
                            </center>
                          <?php } ?>

                        </div>
                        </div>
                        <div class="col-md-10 col-md-offset-1" >
                        <div class="panel panel-default" style="width:100%;background-color:hsla(100,100%,100%,0.3);">
                        <center>Birthday</center>
                        <time datetime="2014-09-20" class="icon">
                        <?php
                         $data = explode("/",Auth::user()->birthday);
                         print '<em>year '.$data[2].'</em>';
                         if($data[1]==1) print '<strong>January</strong>';
                         if($data[1]==2) print '<strong>February</strong>';
                         if($data[1]==3) print '<strong>March</strong>';
                         if($data[1]==4) print '<strong>April</strong>';
                         if($data[1]==5) print '<strong>May</strong>';
                         if($data[1]==6) print '<strong>June</strong>';
                         if($data[1]==7) print '<strong>July</strong>';
                         if($data[1]==8) print '<strong>August</strong>';
                         if($data[1]==9) print '<strong>September</strong>';
                         if($data[1]==10) print '<strong>October</strong>';
                         if($data[1]==11) print '<strong>November</strong>';
                         if($data[1]==12) print '<strong>December</strong>';
                         print '<span>'.$data[0].'</span>';
                        ?>
                          </time>
                      </div>
                      </div>

                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 " >
          <div class="panel panel-default" style="background-color:{{ $custom->linecolor2 }};border-color:{{ $custom->linecolor }};" style="background-image: url('{{ $custom->texture }}');">
                <!-- <div class="panel-heading">Dashboard</div> -->
                <div class="panel-body " >
                      <div class="col-md-12" style="background-image: url('{{ $custom->texture }}');">
                        <div class="panel-body">
                          <center>
                            <div class="panel panel-default" style="background-color:hsla(100,100%,100%,0.3);">
                              <h4>My App</h4>
                            </div>
                            </center>
                          <hr>
                          <div class="col-md-10 col-md-offset-1" >
                          <div class="panel panel-default" style="width:100%;background-color:hsla(100,100%,100%,0.3);">
                           <center><br>
                             <?php
                             use App\service;
                             $data = service::find(Auth::user()->id);
                             if($data->mysite)
                                  print '<img src="img/icon/1.png"  width="110px" height="110px" >&nbsp;&nbsp;';
                             if($data->myfile)
                                  print '<img src="img/icon/2.png"  width="110px" height="110px" >&nbsp;&nbsp;';
                             if($data->myphoto)
                                  print '<img src="img/icon/3.png"  width="110px" height="110px" >';
                             ?>
                           </center>
                        </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 " >
          <div class="panel panel-default" style="background-color:{{ $custom->linecolor2 }};border-color:{{ $custom->linecolor }};" style="background-image: url('{{ $custom->texture }}');">
                <!-- <div class="panel-heading">Dashboard</div> -->
                <div class="panel-body " >
                      <div class="col-md-12" style="background-image: url('{{ $custom->texture }}');">
                        <div class="panel-body">
                          <center>
                            <img class="img-thumbnail" src='{{ $custom->bg }} ' style="object-fit: cover;  width: 150px; height:150px;">
                            <img class="img-thumbnail" src='{{ $custom->cover }} ' style="object-fit: cover;  width:150px; height:150px;">
                            <img class="img-thumbnail" src='{{ $custom->texture }}'  style="object-fit: cover;  width:150px; height:150px;"  >
                          </center>
                        </div>
                      </div>
                </div>
            </div>
        </div>
      </div>
  </div>
</body>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });

});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
</script>
<script>
  $(function() {
    $("#page-wrap a[title]").tooltips();
  });
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
S
</script>
</body>
</html>
@endsection
