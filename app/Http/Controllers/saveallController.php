<?php

namespace App\Http\Controllers;

use App\customprofile;
use App\service;
use App\history_imgprofile;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class saveallController extends Controller
{
  public function uploadprofile()
  {
    //---------------move file to server---------------------
  //
      if(Input::hasFile('files')) {
        $file = Input::file('files');

        if( !is_dir('img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/crop')){ //create folder crop
          mkdir('img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/crop', 0700,true);
        }

        $path = 'img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name;
        $file->move($path,$file->getClientOriginalName());
      }
      //---------------save in database----------------------
      $update = Auth::user();
      $update->path_img = $path.'/'.$file->getClientOriginalName();
      $update->save();
      //-----------------------------------------------------

  }
  public function savecrop(){
  //  img/upload/1st_profile/13_Boss/Jellyfish.jpg
       $img = Input::all();
       $pic = explode("/", $img['db_path']);
       $path = $pic[0].'/'.$pic[1].'/'.$pic[2].'/'.$pic[3].'/crop';
       $namefile = 'crop_profile.jpg';
        $update1 = Auth::user();
        $update1->path_img = $path.'/'.$namefile;
        $update1->save();

        $data = $img['img_path'];
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        file_put_contents($path.'/'.$namefile, $data);
        if(Auth::user()->setup_index == 0){
            return view('setup_profile.showandchange');
        }else{
           return view('setup_profile.changeimg');
        }

    //return $path.'/'.$namefile;
  }
  public function togoskip(){

    $update1 = Auth::user();
    $update1->path_img ="img/texture/default profile picture.png";
    $update1->setup_index = 1 ;
    $update1->save();
    return view('setup_profile.service');
  }
  public function save_pro(){

    $update1 = Auth::user();
    $update1->setup_index = 1;
    $update1->save();
    return view('setup_profile.service');
  }
  public function service_save(){

    $update1 = Auth::user();
    $update1->setup_index = 1;
    $update1->save();
    return view('setup_profile.service');
  }

  public function saveservice(){
    $data = Input::all();
    //$a = service::where('id_fk', 'LIKE', '%'.Auth::user()->id.'%')->get();
    $DB = service::find(Auth::user()->id);
    if($DB == null){
      $DB = new service;
      $DB->id = Auth::user()->id;
      if(isset($data['mysite'])){
        $DB->mysite = $data['mysite'];
      }else{
        $DB->mysite = 0;
      }
      if(isset($data['myfile'])){
        $DB->myfile = $data['myfile'];
      }else{
        $DB->myfile = 0;
      }
      if(isset($data['myphoto'])){
        $DB->myphoto = $data['myphoto'];
      }else{
        $DB->myphoto = 0;
      }
      $DB->save();
      $b = 'new';
    }else{
      if(isset($data['mysite'])){
        $DB->mysite = $data['mysite'];
      }else{
        $DB->mysite = 0;
      }
      if(isset($data['myfile'])){
        $DB->myfile = $data['myfile'];
      }else{
        $DB->myfile = 0;
      }
      if(isset($data['myphoto'])){
        $DB->myphoto = $data['myphoto'];
      }else{
        $DB->myphoto = 0;
      }
        $DB->save();
      $b = 'update';
    }

    //return view('testpull', ['mysite' => $b]);
    $custom = customprofile::find(Auth::user()->id);
    if($custom == ''){
       return view('installprofile');
    }else{
      return view('home', ['custom' => $custom]);
    }
  }
}
