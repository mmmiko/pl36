<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\customprofile;
use App\service;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $custom = customprofile::find(Auth::user()->id);
       if($custom == ''){
          return view('installprofile');
       }else{
         return view('home', ['custom' => $custom]);
       }
    }
    public function installprofile()
    {
       $save = new customprofile;
       $save->id = Auth::user()->id;
       $save->bg = "img/bg/test2.png";
       $save->cover = "img/texture/world-map-gray.png";
       $save->texture = "img/texture/bg-6.jpg";
       $save->color = "#fff";
       $save->linecolor = "#fff";
       $save->linecolor2 = "#fff";
       $save->save();
       $newser = service::find(Auth::user()->id);
       if($newser == ''){
         $newser = new service;
         $newser->id = Auth::user()->id;
         $newser->mysite = 1;
         $newser->myfile = 0;
         $newser->myphoto = 0;
         $newser->save();
    }
      $custom = customprofile::find(Auth::user()->id);
      return view('home', ['custom' => $custom]);
    }
    public function homecustom()
    {
       $custom = customprofile::find(Auth::user()->id);
        return view('customprofile', ['custom' => $custom]);
    }
    public function test()
    {
      $data = Input::all();
      return view('tool_page.colorpicker', ['data' => $data['color']]);
    }
    public function show()
    {
      $savecustom = customprofile::find(Auth::user()->id);
      $data = Input::all();
      if($data['check'] == 1){
        $savecustom->linecolor = $data['colorborder'];
        $savecustom->save();
      }
      if($data['check']== 2){
        $savecustom->linecolor2 = $data['colorbg'];
        $savecustom->save();
      }
      $custom = customprofile::find(Auth::user()->id);
      return view('customprofile', ['custom' => $custom]);
    }

    /*-----------------------------------------------------------*/
    public function customcover()
    {
       $custom = customprofile::find(Auth::user()->id);
        return view('customprofile.uploadcover', ['custom' => $custom]);
    }
    public function custombg()
    {
       $custom = customprofile::find(Auth::user()->id);
        return view('customprofile.uploadbg', ['custom' => $custom]);
    }
    public function customtexture()
    {
       $custom = customprofile::find(Auth::user()->id);
        return view('customprofile.uploadtexture', ['custom' => $custom]);
    }
    /*-----------------------------------------------------------*/

}
