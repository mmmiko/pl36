<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\custommysite;
use App\customprofile;
use App\service;
use Auth;
use App\Http\Requests;


use App\User;
use App\Album;
use App\PostAlbum;
use App\MyStatus;
use DB;
use DateTime;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class MysiteController extends Controller
{
  public function index()
  {
     $customs = custommysite::find(Auth::user()->id);
     $id = Auth::user()->id;
     $objs = DB::select( DB::raw("SELECT * FROM mystatus WHERE id = '$id' ORDER BY created_at DESC ") );
     $ObjAlbum = DB::select( DB::raw("SELECT * FROM albums WHERE id = '$id' ORDER BY created_at DESC ") );
     $ObjPostAlbum = DB::select( DB::raw("SELECT * FROM postalbums WHERE id = '$id' ORDER BY created_at DESC ") );
     $idshowAlbum = 0;

       if($customs->idshow!=0){
            $idshowAlbum =$customs->idshow;
       }


     $data['objs'] = $objs;
     $data['objAlbum'] = $ObjAlbum;
     $data['PostAlbum'] = $ObjPostAlbum;
     $data['method'] = "post";
     $data['url'] = url('mysite/');
     $data['customs'] = $customs;
     $data['idshow'] = $idshowAlbum;


     if($customs == ''){
        return view('installmysite');
     }else{
       return view('mysite.mainmysite', $data);
     }
  }
  public function installmysite()
  {
     $customs = new custommysite;
     $customs->id = Auth::user()->id;
     $customs->position = '[
         {
             "x": 0,
             "y": 0,
             "width": 3,
             "height": 3
         },
         {
             "x": 3,
             "y": 0,
             "width": 9,
             "height": 3
         },
         {
             "x": 0,
             "y": 3,
             "width": 9,
             "height": 3
         },
         {
             "x": 9,
             "y": 3,
             "width": 3,
             "height": 3
         },
         {
             "x": 0,
             "y": 6,
             "width": 9,
             "height": 11
         },
         {
             "x": 9,
             "y": 6,
             "width": 3,
             "height": 11
         }
     ]';
     $customs->indextolist = '[0,1,2,3,4,5]';
     $customs->save();
       return redirect(url('mysite/'));

  }
  public function resetmysite()
  {
     $customs = custommysite::find(Auth::user()->id);
     $customs->id = Auth::user()->id;
     $customs->position = '[
         {
             "x": 0,
             "y": 0,
             "width": 3,
             "height": 3
         },
         {
             "x": 3,
             "y": 0,
             "width": 9,
             "height": 3
         },
         {
             "x": 0,
             "y": 3,
             "width": 9,
             "height": 3
         },
         {
             "x": 9,
             "y": 3,
             "width": 3,
             "height": 3
         },
         {
             "x": 0,
             "y": 6,
             "width": 9,
             "height": 11
         },
         {
             "x": 9,
             "y": 6,
             "width": 3,
             "height": 11
         }
     ]';
     $customs->indextolist = '[0,1,2,3,4,5]';
     $customs->save();
     return view('mysite.custommysite');

  }
  public function savecustommysite()
  {
    $data = Input::all();
    $customs = custommysite::find(Auth::user()->id);
    $customs->position = $data['position'];
    $customs->indextolist = $data['indextolist'];
    $customs->save();

    return redirect(url('mysite/'));
  }
  public function store(Request $request)
  {
      $id = $request['iduser'];
      $ObjStatus = new MyStatus();
      $status =  $request['status'];
      $nameuser = $request['idname'];
      $iduser = $request['iduser'];
      $file = $request->file('file');


      if($request->hasFile('file')){
        $this->validate($request ,[
          'file' => 'required'
        ]);
        $file = $request->file('file');
        $nameimgFile = $request->file('file')->getClientOriginalName();
        $path = 'pl36/mySite/'.Auth::user()->id.'_'.$nameuser;
        $nameimg = str_random(10).''.$nameimgFile;
        $ObjStatus->id = $request['iduser'];
        $ObjStatus->status = $status;
        $ObjStatus->nameimg = $nameimg;
        $ObjStatus->pathimg = $path;
        $ObjStatus->typefile = Input::file('file')->getMimeType();
        $ObjStatus->save();
        $file->move('pl36/mySite/'.Auth::user()->id.'_'.$nameuser,$nameimg);
      }else {
        $this->validate($request ,[
          'status' => 'required',
        ]);
        $ObjStatus->id = $request['iduser'];
        $ObjStatus->status = $status;
        $ObjStatus->save();
      }
        return redirect(url('mysite/'));
  }
  public function mutiupload(Request $request){

    if(!$request->hasFile('filemuti'))   {
      Session::flash('fail', 'You do not have to select files.');
      return redirect(url('mysite/'));

    }
    else {
      $files = $request->file('filemuti');
      $arrlength = count($files);
      $id =  $request['iduser'];
      $nameuser = $request['idname'];
      $now = Carbon::now();
      $namalbum = $id.'-'.$now;
      $path = 'pl36/mySite/'.$id.'_'.$nameuser.'/tmp';
      $deleteDirectory = File::deleteDirectory($path);
      $ObjPostAlbum = new PostAlbum();
      $ObjPostAlbum->id = $id;
      $ObjPostAlbum->save();
      $objs = DB::select( DB::raw("SELECT * FROM postalbums WHERE id = '$id' ORDER BY created_at DESC ") );
      $IdLastPost = $objs[0]->IdPostAlbums;

      File::makeDirectory('pl36/mySite/'.$id.'_'.$nameuser.'/'.$IdLastPost, 0775, true);
          for($i = 0; $i < $arrlength; $i++) {
              $ObjAlbum = new Album();


              $type = $files[$i]->getMimeType();
              $dotfile = explode("/", $type);
              $nameimg = str_random(15).''.$IdLastPost.'.'.$dotfile[1];
              $ObjAlbum->namalbum = $namalbum;
              $ObjAlbum->id = $id;
              $ObjAlbum->nameimg = $nameimg;
              $ObjAlbum->IdPostAlbums = $IdLastPost;
              $ObjAlbum->pathimg = $path;
              $ObjAlbum->typefile = $type;
              $ObjAlbum->save();
              $files[$i]->move('pl36/mySite/'.Auth::user()->id.'_'.$nameuser.'/tmp',$nameimg);
          }
          $filesSel = DB::select( DB::raw("SELECT * FROM albums WHERE namalbum = '$namalbum' ORDER BY created_at DESC ") );
          $data['files'] = $filesSel;
          $data['idAlbum'] = $IdLastPost;
          return view('mysite.UploadAlbum',$data);
    }
  }
  public function uploadMutifile(Request $request){
    $waitCon = $request['can'];
    $idAb = $request['idAb'];

    if($waitCon=="save"){
      $IdDel = $request['delimg'];
      $NameAlbum = $request['NameAlbum'];
      $Description = $request['Description'];
      $bday = $request['bday'];
      $coutDel = $request['coutDel'];


      $filesAlbum = DB::select( DB::raw("SELECT * FROM albums WHERE IdPostAlbums = '$idAb' ORDER BY created_at DESC ") );
      $iduser = $filesAlbum[0]->id;


      $length = count($filesAlbum);
      for($i = 0 ; $i < $coutDel ; $i++){
          $delId = explode(",", $IdDel);
              for($j = 0 ; $j < $length ; $j++){
                  if($delId[$i+1]==$filesAlbum[$j]->IdAlbum){
                      DB::table('albums')->where('IdAlbum', '=', $delId[$i+1])->delete();
                      $delfile = $filesAlbum[$j]->pathimg.'/'.$filesAlbum[$j]->nameimg;
                      File::Delete($delfile);
                      break;
                  }
              }
      }

              if($NameAlbum=="" ){
                $NameAlbum = "Untitled Album";
              }
              if($bday=="" ){
                $now = Carbon::now();
                $bday = $now;
              }
              DB::update("update postalbums set nameAlbum = '".$NameAlbum."' , description = '".$Description."'  , created_at = '".$bday."' , confrim = '1' WHERE IdPostAlbums ='".$idAb."'" );

              $findAlbum = DB::select( DB::raw("SELECT * FROM albums WHERE IdPostAlbums = '$idAb' ORDER BY created_at DESC ") );
              $name = Auth::user()->name;

              $patch = 'pl36/mySite/'.$findAlbum[0]->id.'_'.$name.'/'.$findAlbum[0]->IdPostAlbums.'/';
              $lengthfind = count($findAlbum);
                  for($i = 0; $i < $lengthfind; $i++) {
                      $delfile = $findAlbum[0]->pathimg.'/'.$findAlbum[$i]->nameimg;
                      $target = 'pl36/mySite/'.$findAlbum[0]->id.'_'.$name.'/'.$findAlbum[0]->IdPostAlbums.'/'.$findAlbum[$i]->nameimg;
                      File::copy($delfile, $target);
                      File::Delete($delfile);
                      DB::update("update albums set pathimg = '".$patch."'  , namalbum = '".$NameAlbum."' , description = '".$Description."' WHERE IdAlbum ='".$findAlbum[$i]->IdAlbum."'" );
                  }
    }else {

      $objAlbum = DB::select( DB::raw("SELECT * FROM albums WHERE IdPostAlbums = '$idAb' ORDER BY created_at DESC ") );
      $arrlength = count($objAlbum);
          DB::table('postalbums')->where('IdPostAlbums', '=', $idAb)->delete();
        for($i = 0; $i < $arrlength; $i++) {
          DB::table('albums')->where('IdAlbum', '=', $objAlbum[$i]->IdAlbum)->delete();
        }
        $directory = $objAlbum[0]->pathimg;
        $name = Auth::user()->name;
        $deleteDirectory = File::deleteDirectory($directory);
        $deleteDirectorytmp = File::deleteDirectory('pl36/mySite/'.$objAlbum[0]->id.'_'.$name.'/'.$objAlbum[0]->IdPostAlbums);
    }

      return redirect(url('mysite/'));

  }
  public function editPost(Request $request)
  {
      $IdEdit = $request['idpost'];
      $objs = DB::select( DB::raw("SELECT * FROM mystatus WHERE IdStatus = '$IdEdit'") );
      $edit = $request['edit'];
      $delId = $request['delimg'];

      if($objs[0]->typefile==""){
            if($edit==""){
                    Session::flash('test', 'This post appears to be blank. Please write something or attach a link or photo to post.');
                    return redirect(url('mysite/'));
              }else {
                if($delId!=""){
                    $delfile = $objs[0]->pathimg.'/'.$objs[0]->nameimg;
                    File::Delete($delfile);
                    DB::update("update mystatus set pathimg = '', nameimg = '' , typefile ='' , status = '".$edit."', version = version+'1' WHERE IdStatus ='".$IdEdit."'" );

                }else {
                  DB::update("update mystatus set status = '".$edit."' ,version =version+'1'  WHERE IdStatus ='".$IdEdit."'" );
                }
            }
      }else {
            if($delId!=""){
                if($edit==""){
                  Session::flash('test', 'This post appears to be blank. Please write something or attach a link or photo to post.');
                  return redirect(url('mysite/'));
                }else {
                  $delfile = $objs[0]->pathimg.'/'.$objs[0]->nameimg;
                  File::Delete($delfile);
                  DB::update("update mystatus set pathimg = '', nameimg = '' , typefile ='' , status = '".$edit."', version = version+'1' WHERE IdStatus ='".$IdEdit."'" );
                }
            }
            else {
              //if($edit==""){
              //  Session::flash('test', 'This post appears to be blank. Please write something or attach a link or photo to post.');
                //return redirect(url('mysite/'));
              //}else {
                DB::update("update mystatus set status = '".$edit."' ,version =version+'1'  WHERE IdStatus ='".$IdEdit."'" );
              //}
            }
      }

    return redirect(url('mysite/'));
  }

  public function stepEditAlbum($idSelc)
  {
      $id = $idSelc;
      $ObjAlbum = DB::select( DB::raw("SELECT * FROM albums WHERE IdPostAlbums = '$id' ORDER BY created_at DESC ") );
      $data['objAlbum'] = $ObjAlbum;
      $data['method'] = "post";
      $data['url'] = url('editPhoto/');

      return view('mysite.EditAlbum',$data);
  }
  public function editAlbum(Request $request){
      $checkDel = $request['check'];

      if($checkDel=="delAll"){
        $idDel = $request['idfordel'];
        $iduse = $request['iduse'];
        $objs = DB::select( DB::raw("SELECT * FROM postalbums WHERE IdPostAlbums = '$idDel'") );
        $objAlbum = DB::select( DB::raw("SELECT * FROM albums WHERE IdPostAlbums = '$idDel'") );
        DB::update("update custommysite set idshow = '0' WHERE id ='".$iduse."'" );

        $customs = custommysite::find(Auth::user()->id);
        if($customs->idshow==$objAlbum[0]->IdPostAlbums){
              DB::update("update custommysite set idshow = '0' WHERE id ='".$iduse."'" );
        }
        $arrlength = count($objAlbum);
            DB::table('postalbums')->where('IdPostAlbums', '=', $idDel)->delete();
          for($i = 0; $i < $arrlength; $i++) {
            DB::table('albums')->where('IdAlbum', '=', $idDel)->delete();
          }
          $directory = $objAlbum[0]->pathimg;
          $deleteDirectory = File::deleteDirectory($directory);
          return redirect(url('mysite/'));
      }
      else{
        $IdDel = $request['delimg'];
        $NameAlbum = $request['NameAlbum'];
        $Description = $request['Description'];
        $bday = $request['bday'];
        $coutDel = $request['coutDel'];
        $idAb = $request['idAb'];

        $filesAlbum = DB::select( DB::raw("SELECT * FROM albums WHERE IdPostAlbums = '$idAb' ORDER BY created_at DESC ") );
        $iduser = $filesAlbum[0]->id;
        $namebefore = $filesAlbum[0]->namalbum;
        $datebefore = $filesAlbum[0]->created_at;
        $length = count($filesAlbum);
        for($i = 0 ; $i < $coutDel ; $i++){
            $delId = explode(",", $IdDel);
                for($j = 0 ; $j < $length ; $j++){
                    if($delId[$i+1]==$filesAlbum[$j]->IdAlbum){
                        DB::table('albums')->where('IdAlbum', '=', $delId[$i+1])->delete();
                        $delfile = $filesAlbum[$j]->pathimg.'/'.$filesAlbum[$j]->nameimg;
                        File::Delete($delfile);
                        break;
                    }
                }
        }

        if($NameAlbum=="" ){
          $NameAlbum = $namebefore;
        }
        if($bday=="" ){
          $bday = $datebefore;
        }
        DB::update("update postalbums set nameAlbum = '".$NameAlbum."' , description = '".$Description."', created_at = '".$bday."', updated_at = '".$bday."'  WHERE IdPostAlbums ='".$idAb."'" );
        $findAlbum = DB::select( DB::raw("SELECT * FROM albums WHERE IdPostAlbums = '$idAb' ORDER BY created_at DESC ") );
        $lengthfind = count($findAlbum);
            for($i = 0; $i < $lengthfind; $i++) {
                DB::update("update albums set IdPostAlbums = '".$idAb."'  , namalbum = '".$NameAlbum."' , description = '".$Description."', created_at = '".$bday."', updated_at = '".$bday."' , version = '1' WHERE IdAlbum ='".$findAlbum[$i]->IdAlbum."'" );
            }
      }
        return redirect(url('mysite/'));
  }

  public function deleteStatus($name,$id)
  {

      if($name=="del"){
          $objs = DB::select( DB::raw("SELECT * FROM mystatus WHERE IdStatus = '$id'") );
          DB::table('mystatus')->where('IdStatus', '=', $id)->delete();
          $delfile = $objs[0]->pathimg.'/'.$objs[0]->nameimg;
          File::Delete($delfile);
      }
      else {
            $objs = DB::select( DB::raw("SELECT * FROM postalbums WHERE IdPostAlbums = '$id'") );
            $objAlbum = DB::select( DB::raw("SELECT * FROM albums WHERE IdPostAlbums = '$id'") );
            $arrlength = count($objAlbum);
                DB::table('postalbums')->where('IdPostAlbums', '=', $id)->delete();
              for($i = 0; $i < $arrlength; $i++) {
                DB::table('albums')->where('IdAlbum', '=', $objAlbum[$i]->IdAlbum)->delete();
              }
              $directory = $objAlbum[0]->pathimg;
              $deleteDirectory = File::deleteDirectory($directory);
      }

        return redirect(url('mysite/'));
  }
  public function setShowSlide($id,$iduser)
  {
        $idshow = $id;
        $iduse = $iduser;
        DB::update("update custommysite set idshow = '".$idshow."' WHERE id ='".$iduse."'" );
        return redirect(url('mysite/'));
  }



}
