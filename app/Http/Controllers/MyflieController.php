<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\My_file;
use App\User;
use DB;
use Auth;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;



class MyflieController extends Controller
{

     public function showindex()
     {
       $id = Auth::user()->id;
       $objs = DB::select( DB::raw("SELECT * FROM my_flies WHERE id = '$id'") );
       $data['objs'] = $objs;
       return view('Myfile.editfile',$data);
     }

    public function index()
    {
      $id = Auth::user()->id;
      $files = DB::select( DB::raw("SELECT * FROM my_flies WHERE id = '$id'") );
      $data['a'] = $files;
      return view('Myfile.MyfileHome',$data);
    }

    public function create()
    {
        $data['method'] = "post";
        $data['url'] = url('myflie/');
        return view('Myfile.upload',$data);
    }


 public function store(Request $request)
    {
      $objuser = new User();

      if ($request->hasFile('file')) {
          $nameuser = $objuser->name = $request['idname'];
          $file = $request->file('file');
          $arrlength = count($file);

            for($i = 0; $i < $arrlength; $i++) {
                $obj = new My_file();
                $nameimg = $file[$i]->getClientOriginalName();
                $bytes = File::size($file[$i]);
                $type = $file[$i]->getMimeType();
                $path = 'pl36/myfile/'.Auth::user()->id.'_'.$nameuser;
                $obj->id = $request['iduser'];
                $obj->namefile = $nameimg;
                $obj->faceName = $nameimg;
                $obj->pathfile = $path;
                $obj->typefile = $type;

                if ($bytes >= 1073741824)
                {
                    $bytes = number_format($bytes / 1073741824, 2) . ' GB';
                }
                elseif ($bytes >= 1048576)
                {
                    $bytes = number_format($bytes / 1048576, 2) . ' MB';
                }
                elseif ($bytes >= 1024)
                {
                    $bytes = number_format($bytes / 1024, 2) . ' kB';
                }
                elseif ($bytes > 1)
                {
                    $bytes = $bytes . ' bytes';
                }
                elseif ($bytes == 1)
                {
                    $bytes = $bytes . ' byte';
                }
                else
                {
                    $bytes = '0 bytes';
                }

                $obj->sizefile = $bytes;
                $obj->save();
                $file[$i]->move('pl36/myfile/'.Auth::user()->id.'_'.$nameuser,$nameimg);
            }

      }
      else{
        Session::flash('upfile', 'You do not have to select files.');
        $data['method'] = "post";
        $data['url'] = url('myflie/');
        return view('Myfile.upload',$data);

      }

        return redirect(url('myflie'));
    }

    public function editfile(Request $request)
    {
      $id = $request['idfile'];
      // $obj = DB::select( DB::raw("SELECT * FROM my_flies WHERE myfile_id = '$id'") );
      // $data['obj'] =  $obj;
      $data['url'] = url('myflie/'.$id);
      $data['method'] = "put";
      return view('Myfile.edituploadfile',$data);
    }

    public function update(Request $request, $id)
    {

      $obj = DB::select( DB::raw("SELECT * FROM my_flies WHERE myfile_id = '$id'") );
      $typefile = explode(".", $obj[0]->namefile);
      $NameFile = $request['NameFile'].'.'.$typefile[1];

      $this->validate($request ,[
        'NameFile' => 'required|min:5'
      ]);

      if($obj[0]->namefile != $obj[0]->faceName){
        $delfile = $obj[0]->pathfile.'/'.$obj[0]->faceName;
        $target =$obj[0]->pathfile.'/'.$NameFile;
        File::copy($delfile, $target);
        File::Delete($delfile);

      }
      else {
        $delfile = $obj[0]->pathfile.'/'.$obj[0]->namefile;
        $target =$obj[0]->pathfile.'/'.$NameFile;
        File::copy($delfile, $target);
        File::Delete($delfile);
      }


      DB::update("update my_flies set faceName = '".$NameFile."' ,version =version+'1'  WHERE myfile_id ='".$id."'" );
      return redirect(url('editmyflie/'));
    }

    public function deletefile(Request $request)
    {
      $numfile =  $request['idfile'];
      $id =  $request['iduse'];
      $objs = DB::select( DB::raw("SELECT * FROM my_flies WHERE myfile_id = '$numfile'") );
      if($objs[0]->namefile != $objs[0]->faceName){
              $delfile = $objs[0]->pathfile.'/'.$objs[0]->faceName;
              File::Delete($delfile);
      }
      else {
            $delfile = $objs[0]->pathfile.'/'.$objs[0]->namefile;
              File::Delete($delfile);
      }
      DB::table('my_flies')->where('myfile_id', '=', $numfile)->delete();
      //return redirect(url('editmyflie/'));
      $objck = DB::select( DB::raw("SELECT * FROM my_flies WHERE id = '$id'") );
      echo count($objck);
      if(count($objck)==0){
        return redirect(url('myflie/'));
      }
      else {
        return redirect(url('editmyflie/'));
      }

    }
    public function delall(Request $request)
    {
        $id = $request['idUser'];
        $del = $request['delA'];
        if($del=="delAll"){

            $objs = DB::select( DB::raw("SELECT * FROM my_flies WHERE id = '$id'") );
            $many  = count($objs);
              for($i = 0 ; $i < $many ; $i++){
                $delfile = $objs[$i]->pathfile.'/'.$objs[$i]->faceName;
                File::Delete($delfile);
                DB::table('my_flies')->where('myfile_id', '=', $objs[$i]->myfile_id)->delete();
              }
              return redirect(url('myflie/'));
        }
        else{
          $delsel = $request['seclId'];
          $coutDel = $request['coutDel'];
          // $new_array[$coutDel];
          // $iddelappove = "";
          //   for($i = 0 ; $i < $coutDel ; $i++){
          //       $sprit = explode(":", $delsel);
          //       $idselection = $sprit[$i+1];
          //       $coutck=0;
          //       echo $idselection." check1 <br>";
          //       for($j = 0 ; $j < $coutDel ; $j++){
          //         $spritck = explode(":", $delsel);
          //         $secck = $spritck[$j+1];
          //           echo $secck."<br>";
          //           if($idselection==$secck){
          //             $coutck++;
          //
          //           }
          //       }
          //       echo "string".$coutck."<br>";
              //  echo $i+"ssssssssssssssss : ".$coutck." <br>";
                // if($coutck%2==1){
                //   echo "string";
                // $iddelappove += ":"+$secck;
                // echo $iddelappove;
                // }
            // }
          for($i = 0 ; $i < $coutDel ; $i++){
                $sprit = explode(":", $delsel);
                $idselection = $sprit[$i+1];
                $objs = DB::select( DB::raw("SELECT * FROM my_flies WHERE myfile_id = '$idselection'") );

                $delfile = $objs[0]->pathfile.'/'.$objs[0]->faceName;
                File::Delete($delfile);
                DB::table('my_flies')->where('myfile_id', '=', $idselection)->delete();
          }
        }
           return redirect(url('editmyflie/'));
    }

    public function limitSpeed(Request $request)
    {
      $numfile =$request['limit'];
      $id =$request['idfile'];
      if($numfile==0){
        $objs = DB::select( DB::raw("SELECT * FROM my_flies WHERE myfile_id = '$id'") );
        $local_file = $objs[0]->pathfile.'/'.$objs[0]->faceName;
        $download_file = $objs[0]->faceName;
        $size = File::size($local_file);

        $download_rate = 50;

            set_time_limit(0);
            header('Cache-control: private');
            header('Content-Type: application/octet-stream');
            header('Content-Length: '.$size);
            header('Content-Disposition: filename='.$download_file);
            flush();
            $file = fopen($local_file, "r");
            while(!feof($file)) {
                print fread($file, round($download_rate * 1024));
                flush();
                sleep(1);
            }
            fclose($file);
      }
      else {
        $objs = DB::select( DB::raw("SELECT * FROM my_flies WHERE myfile_id = '$id'") );
        $local_file = $objs[0]->pathfile.'/'.$objs[0]->faceName;
        $download_file = $objs[0]->faceName;
        $size = File::size($local_file);

        $download_rate = 50000;

            set_time_limit(0);
            header('Cache-control: private');
            header('Content-Type: application/octet-stream');
            header('Content-Length: '.$size);
            header('Content-Disposition: filename='.$download_file);
            flush();
            $file = fopen($local_file, "r");
            while(!feof($file)) {
                print fread($file, round($download_rate * 1024));
                flush();
                sleep(1);
            }
            fclose($file);
      }
        return redirect(url('editmyflie/'.$objs[0]->id));
    }

}
