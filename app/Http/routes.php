<?php
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DateTime;
use App\customprofile;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

          Route::auth();
          Route::get('/', function () {
              return view('welcome');
          });
          //--------------------------------setup profile --------------------------------
          Route::get('/skip', 'saveallController@togoskip');
          Route::post('/savecrop', 'saveallController@savecrop');
          //Route::post('/submit', 'saveallController@uploadprofile');
          Route::get('/save_pro', 'saveallController@save_pro');
          Route::get('/saveservice', 'saveallController@saveservice');

          Route::get('/testpull', 'saveallController@testpull');

          Route::get('/setup', function () {
              return view('setup_profile.default_profile');
          });
          Route::get('/cropimg', function () {
              return view('tool_page.cropimg');
          });
          Route::get('/service', function () {
              return view('setup_profile.service');
          });
          //------------------------------------end --------------------------------------

          //------------------------------------custom profile---------------------------------------------------------------------------------------------------------------------------------
          Route::get('/changeimg', function () {
              return view('setup_profile.changeimg');
          });
          Route::get('/changeservice', function () {
              return view('setup_profile.showandchange');
          });
          Route::post('/show', 'HomeController@show');
          Route::get('/installprofile', 'HomeController@installprofile');
          Route::get('/customcover', 'HomeController@customcover');
          Route::get('/custombg', 'HomeController@custombg');
          Route::get('/customtexture', 'HomeController@customtexture');

          Route::post('uploadcover', function () {
              var_dump(Input::all());
              if(Input::hasFile('files')) {
                $file = Input::file('files');
                if( !is_dir('img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/uploadcover')){ //create folder crop
                  mkdir('img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/uploadcover', 0700,true);
                }
                $path = 'img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/uploadcover';
                $file->move($path,$file->getClientOriginalName());
              }
              //---------------save in database----------------------
              $update = customprofile::find(Auth::user()->id);
              $update->cover = $path.'/'.$file->getClientOriginalName();
              $update->save();
              //-----------------------------------------------------
          });
          Route::post('uploadbg', function () {
              var_dump(Input::all());
              if(Input::hasFile('files')) {
                $file = Input::file('files');
                if( !is_dir('img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/uploadbg')){ //create folder crop
                  mkdir('img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/uploadbg', 0700,true);
                }
                $path = 'img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/uploadbg';
                $file->move($path,$file->getClientOriginalName());
              }
              //---------------save in database----------------------
              $update = customprofile::find(Auth::user()->id);
              $update->bg = $path.'/'.$file->getClientOriginalName();
              $update->save();
              //-----------------------------------------------------
          });
          Route::post('uploadtexture', function () {
              var_dump(Input::all());
              if(Input::hasFile('files')) {
                $file = Input::file('files');
                if( !is_dir('img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/uploadtexture')){ //create folder crop
                  mkdir('img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/uploadtexture', 0700,true);
                }
                $path = 'img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/uploadtexture';
                $file->move($path,$file->getClientOriginalName());
              }
              //---------------save in database----------------------
              $update = customprofile::find(Auth::user()->id);
              $update->texture = $path.'/'.$file->getClientOriginalName();
              $update->save();
              //-----------------------------------------------------
          });

          //--------------------------custom theme profile--------------------------------
          Route::get('/home', 'HomeController@index');
          Route::get('/homecustom', 'HomeController@homecustom');
          Route::post('/uploadprofile', 'saveallController@uploadprofile');
          //------------------------------------end ----------------------------------------------------------------------------------------------------------------------------------------------

          //------------search-----------------------
          Route::get('/search', function () {
              print "search";
          });



          Route::post('submit', function () {
          //---------------move file to server---------------------
          //
            var_dump(Input::all());
            if(Input::hasFile('files')) {
              $file = Input::file('files');
              if( !is_dir('img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/crop')){ //create folder crop
                mkdir('img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name.'/crop', 0700,true);
              }
              $path = 'img/upload/1st_profile'.'/'.Auth::user()->id.'_'.Auth::user()->name;
              $file->move($path,$file->getClientOriginalName());
            }
            //---------------save in database----------------------
            $update = Auth::user();
            $update->path_img = $path.'/'.$file->getClientOriginalName();
            $update->save();
            //-----------------------------------------------------
          });
          //-------------------youtube----------------------------
          Route::resource('/managelink','youtubeController@managelink');
          Route::get('/audio0','youtubeController@index');

          //-----------------myfile--------------------------
          Route::resource('/myflie', 'MyflieController');
          Route::get('create/', 'MyflieController@create');
          Route::get('/editmyflie', 'MyflieController@showindex');
          Route::post('edit/', 'MyflieController@editfile');
          Route::post('/editmyflie/delete/', 'MyflieController@deletefile');
          Route::post('/editmyflie/download/', 'MyflieController@limitSpeed');
          Route::post('delall/', 'MyflieController@delall');

          //------------------------mysite--------------------------------
          Route::resource('/mysite', 'MysiteController');
          Route::post('mutiupload', 'MysiteController@mutiupload');
          Route::post('Savemutiupload', 'MysiteController@uploadMutifile');
          Route::post('mysite/editPost', 'MysiteController@editPost');
          Route::get('{id}', 'MysiteController@stepEditAlbum');
          Route::post('editPhoto', 'MysiteController@editAlbum');
          Route::get('setShow/{id}/{id2}', 'MysiteController@setShowSlide');
          Route::get('mysite/{num?}/{id}', 'MysiteController@deleteStatus');
          Route::get('/mysite', 'MysiteController@index');
          Route::get('/installmysite', 'MysiteController@installmysite');
          Route::get('/resetmysite', 'MysiteController@resetmysite');
          Route::post('/savecustommysite', 'MysiteController@savecustommysite');
          Route::get('/custommysite', function () {
              return view('mysite.custommysite');
          });
