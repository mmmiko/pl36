<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyStatus extends Model
{
  protected $table = 'mystatus';
  public $timestamps = false;
}
