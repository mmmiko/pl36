<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class custommysite extends Model
{
  protected $table = 'custommysite';
  public $timestamps = false;
}
