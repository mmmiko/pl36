<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostAlbum extends Model
{
  protected $table = 'postalbums';
  public $timestamps = true;
}
