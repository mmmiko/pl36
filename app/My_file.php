<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class My_file extends Model
{
  protected $table = 'my_flies';
  public $timestamps = false;
}
