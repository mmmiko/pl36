<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class history_imgprofile extends Model
{
  protected $table = 'history_imgprofile';
  public $timestamps = false;
}
