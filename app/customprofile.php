<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customprofile extends Model
{
  protected $table = 'customprofile';
  public $timestamps = false;
}
